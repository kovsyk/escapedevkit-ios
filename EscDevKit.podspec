#
#  Be sure to run `pod spec lint EscDevKit.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|
  spec.name         = "EscDevKit"
  spec.version      = "1.2.10"
  spec.summary      = "EscDevKit"
  spec.swift_version = '5.2'
  spec.description  	= "EscDevKit"
  spec.homepage     	= "https://bitbucket.org/kovsyk/escapedevkit-ios/"
  spec.license      	= { :type => "MIT", :file => "LICENSE" }
  spec.author       	= "Escape-Tech"
  spec.source       	= { :git => "https://bitbucket.org/kovsyk/escapedevkit-ios", :tag => "#{spec.version}" }
  spec.platform    	= :ios, "11.0"
  spec.source_files  	= "DevKit/*.{swift}", "DevKit/**/*.{swift}"  

end