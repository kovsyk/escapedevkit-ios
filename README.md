# EscDevKit

## Installation

### CocoaPods

 [CocoaPods](https://cocoapods.org)  is a dependency manager for Cocoa projects. To integrate EscDevKit into your Xcode project using CocoaPods, specify it in your Podfile::

```ruby 
pod 'EscDevKit', :git => 'https://bitbucket.org/kovsyk/escapedevkit-ios'
```

## License

EscDevKit is released under the MIT license.
