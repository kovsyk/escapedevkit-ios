//
//  TestAlertsViewController.swift
//  devkit-demo
//
//  Created by Andrey on 28.09.2021.
//  Copyright © 2021 EscapeTech. All rights reserved.
//

import Foundation
import UIKit

class TestAlertsViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .red
    }
    
    @IBAction func escAlertOneActionTapped(_ sender: Any) {
        let controller = ESCAlertViewController.init(
            title: "Lorem Title",
            message: "Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a",
            style: .alert,
            actions: [
                ESCAlertAction(title: "Action", style: .default, block: {})
            ])
        self.present(controller, animated: true)
    }
    
    @IBAction func escSheetOneActionTapped(_ sender: Any) {
        let controller = ESCAlertViewController.init(
            title: "Lorem Title",
            message: "Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a",
            style: .sheet,
            actions: [
                ESCAlertAction(title: "Action", style: .default, block: {})
            ])
        self.present(controller, animated: true)
    }
    
    @IBAction func sheetOneActionTapped(_ sender: Any) {
        let alertController = UIAlertController(title: "Lorem Title",
                                                message: "Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a",
                                                preferredStyle: .alert)
        let actions = [
            UIAlertAction(title: "Action", style: .default, handler: {_ in }),
        ]
        
        actions.forEach({
            alertController.addAction($0)
        })
        
        self.present(alertController, animated: true)
    }
    
    @IBAction func alertOneActionTapped(_ sender: Any) {
        let alertController = UIAlertController(title: "Lorem Title",
                                                message: "Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a",
                                                preferredStyle: .actionSheet)
        let actions = [
            UIAlertAction(title: "Action", style: .cancel, handler: {_ in })
        ]
        
        actions.forEach({
            alertController.addAction($0)
        })
        
        self.present(alertController, animated: true)
    }
    
    
    @IBAction func escAlertTwoSmallActionsTapped(_ sender: Any) {
        let controller = ESCAlertViewController.init(
            title: "Lorem Title",
            message: "Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a",
            smallIcon: UIImage(named: "AppIcon"),
            style: .alert,
            actions: [
                ESCAlertAction(title: "Action", style: .default, block: {}),
                ESCAlertAction(title: "Cancel", style: .cancel,  block: {})
            ])
        self.present(controller, animated: true)
    }
    
    @IBAction func escSheetTwoSmallActionsTapped(_ sender: Any) {
        let controller = ESCAlertViewController.init(
            title: "Lorem Title",
            message: "Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a",
            smallIcon: UIImage(named: "AppIcon"),
            style: .sheet,
            actions: [
                ESCAlertAction(title: "Action", style: .default, block: {}),
                ESCAlertAction(title: "Cancel", style: .cancel,  block: {})
            ])
        self.present(controller, animated: true)
    }
    
    @IBAction func alertTwoSmallActionsTapped(_ sender: Any) {
        let alertController = UIAlertController(title: "Lorem Title",
                                                message: "Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a",
                                                preferredStyle: .alert)
        let actions = [
            UIAlertAction(title: "Action 1", style: .default, handler: {_ in }),
            UIAlertAction(title: "Action 2", style: .cancel, handler: {_ in })
        ]
        
        actions.forEach({
            alertController.addAction($0)
        })
        
        self.present(alertController, animated: true)
    }
    
    @IBAction func sheetTwoSmallActionsTapped(_ sender: Any) {
        let alertController = UIAlertController(title: "Lorem Title",
                                                message: "Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a",
                                                preferredStyle: .actionSheet)
        let actions = [
            UIAlertAction(title: "Action 1", style: .default, handler: {_ in }),
            UIAlertAction(title: "Action 2", style: .cancel, handler: {_ in })
        ]
        
        actions.forEach({
            alertController.addAction($0)
        })
        
        self.present(alertController, animated: true)
    }
    
    
    @IBAction func escAlertTwoLongActionsTapped(_ sender: Any) {
        let controller = ESCAlertViewController.init(
            title: "Lorem Title",
            message: "Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a",
            smallIcon: UIImage(named: "AppIcon"),
            style: .alert,
            actions: [
                ESCAlertAction(title: "Looooooooong Action", style: .default, block: {}),
                ESCAlertAction(title: "Cancel", style: .cancel,  block: {})
            ])
        self.present(controller, animated: true)
    }
    
    @IBAction func escSheetTwoLongActionsTapped(_ sender: Any) {
        let controller = ESCAlertViewController.init(
            title: "Lorem Title",
            message: "Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a",
            smallIcon: UIImage(named: "AppIcon"),
            style: .sheet,
            actions: [
                ESCAlertAction(title: "Looooooooong Action", style: .default, block: {}),
                ESCAlertAction(title: "Cancel", style: .cancel,  block: {})
            ])
        self.present(controller, animated: true)
    }
    
    @IBAction func alertTwoLongActionsTapped(_ sender: Any) {
        let alertController = UIAlertController(title: "Lorem Title",
                                                message: "Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a",
                                                preferredStyle: .alert)
        let actions = [
            UIAlertAction(title: "Looooooooong Action", style: .default, handler: {_ in }),
            UIAlertAction(title: "Cancel", style: .cancel, handler: {_ in })
        ]
        
        actions.forEach({
            alertController.addAction($0)
        })
        
        self.present(alertController, animated: true)
    }
    
    @IBAction func sheetTwoLongActionsTapped(_ sender: Any) {
        let alertController = UIAlertController(title: "Lorem Title",
                                                message: "Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a",
                                                preferredStyle: .actionSheet)
        let actions = [
            UIAlertAction(title: "Looooooooong Action", style: .default, handler: {_ in }),
            UIAlertAction(title: "Cancel", style: .cancel, handler: {_ in })
        ]
        
        actions.forEach({
            alertController.addAction($0)
        })
        
        self.present(alertController, animated: true)
    }
    
    
    @IBAction func smallEscAlertTextTapped(_ sender: Any) {
        let alertController = ESCTextFieldAlertViewController(
            title: "Lorem Title",
            message: "Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a",
            textFieldText: nil,
            textFieldPlaceholder: "Placeholder",
            actions: [])
        
        alertController.actions = [
            ESCAlertAction(title: "Ok", style: .default, block: { [weak alertController] in
                print(alertController?.text ?? "")
            }),
            ESCAlertAction(title: "Action", style: .default, block: {}),
        ]
        
        self.present(alertController, animated: true) {
            alertController.textField?.becomeFirstResponder()
        }
    }
    
    @IBAction func bigEscAlertTextTapped(_ sender: Any) {
        let alertController = ESCTextFieldAlertViewController(
            title: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            message: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            textFieldText: nil,
            textFieldPlaceholder: "Placeholder",
            actions: [])

        alertController.actions = [
            ESCAlertAction(title: "Ok", style: .default, block: { [weak alertController] in
                print(alertController?.text ?? "")
            }),
            ESCAlertAction(title: "Action 1", style: .default, block: {}),
            ESCAlertAction(title: "Action 2", style: .default, block: {}),
            ESCAlertAction(title: "Action 3", style: .default, block: {}),
            ESCAlertAction(title: "Action 4", style: .default, block: {}),
            ESCAlertAction(title: "Action 5", style: .default, block: {}),
            ESCAlertAction(title: "Action 6", style: .default, block: {}),
            ESCAlertAction(title: "Cancel", style: .cancel,  block: {}),
        ]

        self.present(alertController, animated: true) {
            alertController.textField?.becomeFirstResponder()
        }
    }
    
    @IBAction func smallAlertTextTapped(_ sender: Any) {
        let alert = UIAlertController(title: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor inc", message: "Enter a text", preferredStyle: .alert)

        alert.addTextField { (textField) in
            textField.text = "Some default text"
        }

        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] _ in
            print(alert?.textFields?[0].text)
        }))
        alert.addAction( UIAlertAction(title: "Action", style: .default, handler: {_ in }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func bigAlertTextTapped(_ sender: Any) {
        
        let alert = UIAlertController(title: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum", message: "Enter a text", preferredStyle: .alert)

        alert.addTextField { (textField) in
            textField.text = "Some default text"
        }

        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {_ in }))
        alert.addAction( UIAlertAction(title: "Action 1", style: .default, handler: {_ in }))
        alert.addAction( UIAlertAction(title: "Action 2", style: .default, handler: {_ in }))
        alert.addAction( UIAlertAction(title: "Action 3", style: .default, handler: {_ in }))
        alert.addAction( UIAlertAction(title: "Action 4", style: .default, handler: {_ in }))
        alert.addAction( UIAlertAction(title: "Action 5", style: .default, handler: {_ in }))
        alert.addAction( UIAlertAction(title: "Action 6", style: .default, handler: {_ in }))
        alert.addAction( UIAlertAction(title: "Action 7", style: .default, handler: {_ in }))

        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func smallEscAlertImagesTapped(_ sender: Any) {
        let controller = ESCAlertViewController.init(
            title: "Lorem ipsum dolor sit amet",
            message: "Lorem ipsum dolor sit amet",
            smallIcon: UIImage(named: "AppIcon"),
            mainImage: UIImage(named: "MainImage"),
            style: .alert,
            actions: [
                ESCAlertAction(title: "Action 1", style: .default, block: {}),
                ESCAlertAction(title: "Cancel", style: .cancel,  block: {})
            ])
        self.present(controller, animated: true)
    }
    
    @IBAction func smallEscSheetTextTapped(_ sender: Any) {
        let controller = ESCAlertViewController.init(
            title: "Lorem ipsum dolor sit amet",
            message: "Lorem ipsum dolor sit amet",
            smallIcon: UIImage(named: "AppIcon"),
            mainImage: UIImage(named: "MainImage"),
            style: .sheet,
            actions: [
                ESCAlertAction(title: "Action 1", style: .default, block: {}),
                ESCAlertAction(title: "Cancel", style: .cancel,  block: {})
            ])
        self.present(controller, animated: true)
    }
    
    @IBAction func bigEscAlertImagesTapped(_ sender: Any) {
        let controller = ESCAlertViewController.init(
            title: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
            message: "Lorem ipsum dolor  laborum.Lorem dolor sit aLorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a",
            smallIcon: UIImage(named: "AppIcon"),
            mainImage: UIImage(named: "MainImage"),
            style: .alert,
            actions: [
                ESCAlertAction(title: "Action 1", style: .default, block: {}),
                ESCAlertAction(title: "Action 2", style: .default, block: {}),
                ESCAlertAction(title: "Action 3", style: .default, block: {}),
                ESCAlertAction(title: "Action 4", style: .default, block: {}),
                ESCAlertAction(title: "Action 5", style: .default, block: {}),
                
                ESCAlertAction(title: "Cancel", style: .cancel,  block: {})
            ])
        self.present(controller, animated: true)
    }
    
    @IBAction func bigEscSheetTextTapped(_ sender: Any) {
        let controller = ESCAlertViewController.init(
            title: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
            message: "Lorem ipsum dolor  laborum.Lorem dolor sit aLorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a",
            smallIcon: UIImage(named: "AppIcon"),
            mainImage: UIImage(named: "MainImage"),
            style: .sheet,
            actions: [
                ESCAlertAction(title: "Action 1", style: .default, block: {}),
                ESCAlertAction(title: "Action 2", style: .default, block: {}),
                ESCAlertAction(title: "Action 3", style: .default, block: {}),
                ESCAlertAction(title: "Action 4", style: .default, block: {}),
                ESCAlertAction(title: "Action 5", style: .default, block: {}),
                
                ESCAlertAction(title: "Cancel", style: .cancel,  block: {})
            ])
        self.present(controller, animated: true)
    }
    
    @IBAction func bigEscAlertTapped(_ sender: Any) {
        let controller = ESCAlertViewController.init(
            title: "Lorem Title Lorem ipsum dolor sit amnt mt anim id estsdfaLorem Title Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit sdfaLorem Title Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit sdfaLorem Title Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit sdfaLorem Title Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit sdfaLorem Title Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit  laborum.Lorem dolor sit a Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a",
            message: "Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a",
            smallIcon: UIImage(named: "AppIcon"),
            style: .alert,
            actions: [
                ESCAlertAction(title: "Action 1", style: .default, block: {}),
                ESCAlertAction(title: "Action 2", style: .default, block: {}),
                ESCAlertAction(title: "Action 3", style: .default, block: {}),
                ESCAlertAction(title: "Action 4", style: .default, block: {}),
                ESCAlertAction(title: "Action 5", style: .default, block: {}),
                ESCAlertAction(title: "Action 6", style: .default, block: {}),
                ESCAlertAction(title: "Action 7", style: .default, block: {}),
                ESCAlertAction(title: "Cancel", style: .cancel,  block: {})
            ])
        self.present(controller, animated: true)
    }
    
    @IBAction func bigEscSheetTapped(_ sender: Any) {
        let controller = ESCAlertViewController.init(
            title: "Lorem Title Lorem ipsum dolor sit amnt mt anim id est laborsdfaLorem Title Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit sdfaLorem Title Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit sdfaLorem Title Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit sdfaLorem Title Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit sdfaLorem Title Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit sdfaLorem Title Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit um.Lorem dolor sit a Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a",
            message: "Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a",
            smallIcon: UIImage(named: "AppIcon"),
            style: .sheet,
            actions: [
                ESCAlertAction(title: "Action 1", style: .default, block: {}),
                ESCAlertAction(title: "Action 2", style: .default, block: {}),
                ESCAlertAction(title: "Action 3", style: .default, block: {}),
                ESCAlertAction(title: "Action 4", style: .default, block: {}),
                ESCAlertAction(title: "Action 5", style: .default, block: {}),
                ESCAlertAction(title: "Action 6", style: .default, block: {}),
                ESCAlertAction(title: "Action 7", style: .default, block: {}),
                ESCAlertAction(title: "Cancel", style: .cancel,  block: {})
            ])
        self.present(controller, animated: true)
    }
    
    @IBAction func bigAlertTapped(_ sender: Any) {
        let alertController = UIAlertController(title: "asdfaLorem Title Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit sdfaLorem Title Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit sdfaLorem Title Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit sdfaLorem Title Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit sdfaLorem Title Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit sdfaLorem Title Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit asd dssdff",
                                                message: "asLorem Title Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit afasdfasd fasdf sadf",
                                                preferredStyle: .alert)
        let actions = [
            UIAlertAction(title: "Action 1", style: .default, handler: {_ in }),
            UIAlertAction(title: "Action 2", style: .default, handler: {_ in }),
            UIAlertAction(title: "Action 3", style: .default, handler: {_ in }),
            UIAlertAction(title: "Action 4", style: .default, handler: {_ in }),
            UIAlertAction(title: "Action 5", style: .default, handler: {_ in }),
            UIAlertAction(title: "Action 6", style: .default, handler: {_ in }),
            UIAlertAction(title: "Action 7", style: .cancel, handler: {_ in })
        ]
        
        actions.forEach({
            alertController.addAction($0)
        })
        
        self.present(alertController, animated: true)
    }
    
    @IBAction func bigSheetTapped(_ sender: Any) {
        
        let alertController = UIAlertController(title: "asdfaLorem Title Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsumsdfaLorem Title Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit sdfaLorem Title Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit sdfaLorem Title Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit sdfaLorem Title Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit sdfaLorem Title Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit  dolor sit amnt mt anim id est laborum.Lorem dolor sit asd dssdff",
                                                message: "asLorem Title Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit a Lorem ipsum dolor sit amnt mt anim id est laborum.Lorem dolor sit afasdfasd fasdf sadf",
                                                preferredStyle: .actionSheet)
        let actions = [
            UIAlertAction(title: "Action 1", style: .default, handler: {_ in }),
            UIAlertAction(title: "Action 2", style: .default, handler: {_ in }),
            UIAlertAction(title: "Action 3", style: .default, handler: {_ in }),
            UIAlertAction(title: "Action 4", style: .default, handler: {_ in }),
            UIAlertAction(title: "Action 5", style: .default, handler: {_ in }),
            UIAlertAction(title: "Action 6", style: .default, handler: {_ in }),
            UIAlertAction(title: "Action 7", style: .cancel, handler: {_ in })
        ]
        
        actions.forEach({
            alertController.addAction($0)
        })
        
        self.present(alertController, animated: true)
    }
    
}
