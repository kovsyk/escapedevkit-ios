//
//  MainViewController.swift
//  EscDevKit
//
//  Created by Igor Ovchinnikov on 09.07.2020.
//  Copyright © 2020 EscapeTech. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    @IBOutlet weak var avatarPhotoView: UIImageView!
    @IBOutlet weak var fullPhotoView: UIImageView!

    var module: ESCPhotoSelector!
    var avatarFrameLayer = CAShapeLayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            self.view.backgroundColor = .systemBackground
        }
    }

    @IBAction func showTempAlert(_ sender: Any) {
        
        let controller = ESCAlertViewController.init(
            title: "Lorem Title",
            message: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            smallIcon: UIImage(named: "AppIcon"),
            style: .sheet,
            actions: [
                ESCAlertAction(title: "Action 1", style: .default, block: {}),
                ESCAlertAction(title: "Action 2", style: .default, block: {}),
                ESCAlertAction(title: "Action 3", style: .default, block: {}),
                ESCAlertAction(title: "Action 4", style: .default, block: {}),
                ESCAlertAction(title: "Cancel", style: .cancel,  block: {})
            ])
        self.present(controller, animated: true)
    }
    
    @IBAction func changeAvatarAction(_ sender: Any) {
        module = ESCPhotoSelector.getAvatar(from: self, imageCompletion: { [weak self] (fullImage, avatarImage, avatarPosition) in
            guard let self = self else { return }
            self.avatarPhotoView.image = avatarImage
            self.fullPhotoView.image = fullImage
            DispatchQueue.global(qos: .userInitiated).async {
                let image = fullImage.drawRect(rect: avatarPosition)
                DispatchQueue.main.async {
                    self.fullPhotoView.image = image
                }
            }

            self.fullPhotoView.updateConstraints()
        })
    }
    
    @IBAction func showAlertWithtextField(_ sender: Any) {
        
        let alertController = ESCTextFieldAlertViewController(
            title: "Lorem Title",
            message: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eius.",
            textFieldText: nil,
            textFieldPlaceholder: "Placeholder",
            actions: [])
        
        let actions: [ESCAlertAction] = [
            ESCAlertAction(title: "Ok", style: .default, block: { [weak alertController] in
                print(alertController?.text ?? "")
            }),
            ESCAlertAction(title: "Cancel", style: .cancel,  block: { })
        ]
        
        alertController.actions = actions        
        
        self.present(alertController, animated: true) {
            alertController.textField?.becomeFirstResponder()
        }
    }
}

extension UIImage {
    func drawRect(rect: CGRect) -> UIImage? {
        
        let imageSize = self.size
        
        let w = imageSize.width
        let h = imageSize.height
        
        let rect = CGRect(
            x:      w * rect.origin.x,
            y:      h * rect.origin.y,
            width:  w * rect.size.width,
            height: h * rect.size.height
        )
        
        let scale: CGFloat = 0
        UIGraphicsBeginImageContextWithOptions(imageSize, false, scale)
        let context = UIGraphicsGetCurrentContext()

        self.draw(at: CGPoint.zero)

        context?.setStrokeColor(UIColor.red.cgColor)
        context?.setLineWidth(max(w, h) * 0.01)
        context?.addRect(rect)
        context?.drawPath(using: .stroke)

        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
}
