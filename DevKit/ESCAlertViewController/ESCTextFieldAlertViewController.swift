//
//  ESCTextAlertViewController.swift
//  EscDevKit
//
//  Created by EscapeTech on 15.02.2021.
//  Copyright © 2021 EscapeTech. All rights reserved.
//

import UIKit


public class ESCTextFieldAlertViewController: ESCAlertViewController {
    
    public var text: String? {
        (self.customView as? TextFieldContentView)?.textField.text
    }
    
    public var textField: UITextField? {
        (self.customView as? TextFieldContentView)?.textField
    }
    
    @available(*, unavailable)
    override init(title:                String? = nil,
                  message:              String? = nil,
                  smallIcon:            UIImage? = nil,
                  mainImage:            UIImage? = nil,
                  style:                ESCAlertStyle,
                  autoclose:            Bool = true,
                  actions:              [ESCAlertAction],
                  configurationScheme:  ESCAlertUIConfigurationScheme = .defaultScheme) {
        
        super.init(title: title,
                   message: message,
                   smallIcon: smallIcon,
                   mainImage: mainImage,
                   style: style,
                   actions: actions,
                   configurationScheme: configurationScheme)
    }
    
    @available(*, unavailable)
    override init(attributedTitle:      NSAttributedString? = nil,
                  attributedMessage:    NSAttributedString? = nil,
                  smallIcon:            UIImage? = nil,
                  mainImage:            UIImage? = nil,
                  style:                ESCAlertStyle,
                  autoclose:            Bool = true,
                  actions:              [ESCAlertAction],
                  configurationScheme:  ESCAlertUIConfigurationScheme = .defaultScheme) {
        
        super.init(attributedTitle: attributedTitle,
                   attributedMessage: attributedMessage,
                   smallIcon: smallIcon,
                   mainImage: mainImage,
                   style: style,
                   actions: actions,
                   configurationScheme: configurationScheme)
    }
    
    public init(title:                  String?,
                message:                String?,
                textFieldText:          String?,
                textFieldPlaceholder:   String?,
                actions:                [ESCAlertAction],
                configurationScheme:    ESCAlertUIConfigurationScheme = .defaultScheme) {
        
        super.init(style: .alert, actions: actions, configurationScheme: configurationScheme)

        self.customView = TextFieldContentView(
            title:          title,
            message:        message,
            textFieldText:  textFieldText,
            textFieldPlaceholder:   textFieldPlaceholder,
            configurationScheme:    configurationScheme
        )
        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardDidShowNotification, object: nil)
    }
    
    override func getTopContentHeight(forWidth width: CGFloat) -> CGFloat {
        if let customView = customView {
            return (customView as? TextFieldContentView)?.getContentHeight(width: width) ?? 0
        }
        return 0
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidShowNotification, object: nil)
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        guard let textField = textField else { return }
        contentScrollView.scrollRectToVisible(textField.frame, animated: true)
    }
}

private class TextFieldContentView: UIView, UITextFieldDelegate {

    private var sideOffset: CGFloat = 12.0
    private let spacingBetweenViews: CGFloat = 16.0
    
    var title: String?
    var message: String?
    
    let titleLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.textAlignment = .center
        titleLabel.numberOfLines = 0
        return titleLabel
    }()
    
    let messageLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.textAlignment = .center
        titleLabel.numberOfLines = 0
        return titleLabel
    }()
    
    let textField: UITextField = {
        let textField = UITextField()
        textField.borderStyle = .roundedRect
        return textField
    }()
    
    public init(title: String?, message: String?, textFieldText: String?, textFieldPlaceholder: String?, configurationScheme: ESCAlertUIConfigurationScheme = .defaultScheme) {
        super.init(frame: .zero)
        self.title = title
        self.message = message
        configureTitle(text: title)
        configureMessage(text: message)
        configureTextField(text: textFieldText)
        configure(scheme: configurationScheme)
        addSubview(textField)
        textField.delegate = self
    }

    override init(frame: CGRect) {
        super.init(frame:frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        configureLayout()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        endEditing(true)
    }
    
    func configure(scheme: ESCAlertUIConfigurationScheme) {
        titleLabel.textColor    = scheme.titleColor
        messageLabel.textColor  = scheme.messageColor
        titleLabel.font         = scheme.titleAlertFont
        messageLabel.font       = scheme.messageAlertFont
        textField.tintColor     = scheme.defaultAlertButtonColor
        backgroundColor         = scheme.backgroundColor
    }
    
    func configureTextField(text: String?) {
        textField.autocapitalizationType = UITextAutocapitalizationType.none
        textField.text = text
    }
    
    func configureTitle(text: String?) {
        if text != nil {
            titleLabel.text = text
            addSubview(titleLabel)
        }
    }
    
    func configureMessage(text: String?) {
        if text != nil {
            messageLabel.text = text
            addSubview(messageLabel)
        }
    }
    
    func configureLayout() {
        _ = configureLayoutAndGetHeight()
    }
    
   
    func getContentHeight(width: CGFloat) -> CGFloat {
        return configureLayoutAndGetHeight(width: width, isNeedSetFrame: false)
    }
    
    private func configureLayoutAndGetHeight(width: CGFloat? = nil, isNeedSetFrame: Bool = true) -> CGFloat {
        
        var array: [ViewWithWH] = []
        
        let widthWithoutOffsets = (width != nil) ? width! - 2 * sideOffset : self.frame.width - 2 * sideOffset
        
        if title != nil {
            array.append(ViewWithWH(view: titleLabel, w: widthWithoutOffsets))
        }
        
        if message != nil {
            array.append(ViewWithWH(view: messageLabel, w: widthWithoutOffsets))
        }
        
        array.append(ViewWithWH(view: textField, w: widthWithoutOffsets))
        
        return configureViewsInStackAndGetHeight(verticalSpacing: spacingBetweenViews,
                              viewWithWhArray: array, isNeedSetFrames: isNeedSetFrame)
    }
}
