//
//  ESCAlertViewController.swift
//  EscDevKit
//
//  Created by EscapeTech on 06.03.2020.
//  Copyright © 2020 EscapeTech. All rights reserved.
//

import UIKit

public enum ESCAlertActionStyle {
    case `default`, cancel
}

public struct ESCAlertAction {
    public var title: String
    public var style: ESCAlertActionStyle
    public var block: (() -> Void)?
    
    public init(title: String,
                style: ESCAlertActionStyle,
                block: (() -> Void)?) {
        self.title = title
        self.style = style
        self.block = block
    }
}

public enum ESCAlertStyle {
    case alert, sheet
}

open class ESCAlertViewController: UIViewController {
   
    private var confScheme: ESCAlertUIConfigurationScheme
    
    public var customView: UIView? {
        didSet {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.contentView.subviews.forEach { (subview) in
                    subview.removeFromSuperview()
                }
                if let customView = self.customView {
                    self.contentView.addSubview(customView)
                }
                else {
                    [self.mainImageView, self.titleLabel, self.detailLabel].forEach({ self.contentView.addSubview($0) })
                }
                self.pContentViewLayouts()
            }
        }
    }
    
    public init(title: String? = nil,
                message: String? = nil,
                smallIcon: UIImage? = nil,
                mainImage: UIImage? = nil,
                style: ESCAlertStyle,
                autoclose: Bool = true,
                actions: [ESCAlertAction],
                configurationScheme: ESCAlertUIConfigurationScheme = .defaultScheme) {
        self.confScheme = configurationScheme
        self.actionsCollectionViewFlowLayout = ESCAlertCollectionViewFlowLayout(separatorColor: confScheme.separator)
        self.actionsCollectionView = UICollectionView(frame: .zero, collectionViewLayout: actionsCollectionViewFlowLayout)
        super.init(nibName: nil, bundle: nil)
        self.alertTitle = title
        self.alertDetail = message
        self.mainImage = mainImage
        self.smallIcon = smallIcon
        self.autoclose = autoclose
        self.style = style
        self.actions = actions
    }
    
    public init(attributedTitle: NSAttributedString? = nil,
                attributedMessage: NSAttributedString? = nil,
                smallIcon: UIImage? = nil,
                mainImage: UIImage? = nil,
                style: ESCAlertStyle,
                autoclose: Bool = true,
                actions: [ESCAlertAction],
                configurationScheme: ESCAlertUIConfigurationScheme = .defaultScheme) {
        self.confScheme = configurationScheme
        self.actionsCollectionViewFlowLayout = ESCAlertCollectionViewFlowLayout(separatorColor: confScheme.separator)
        self.actionsCollectionView = UICollectionView(frame: .zero, collectionViewLayout: actionsCollectionViewFlowLayout)
        super.init(nibName: nil, bundle: nil)
        self.attributedTitle = attributedTitle
        self.attributedDetail = attributedMessage
        self.mainImage = mainImage
        self.smallIcon = smallIcon
        self.style = style
        self.autoclose = autoclose
        self.actions = actions
    }
    
    public init(smallIcon: UIImage? = nil,
                mainImage: UIImage? = nil,
                style: ESCAlertStyle,
                autoclose: Bool = true,
                actions: [ESCAlertAction],
                configurationScheme: ESCAlertUIConfigurationScheme = .defaultScheme) {
        self.confScheme = configurationScheme
        self.actionsCollectionViewFlowLayout = ESCAlertCollectionViewFlowLayout(separatorColor: confScheme.separator)
        self.actionsCollectionView = UICollectionView(frame: .zero, collectionViewLayout: actionsCollectionViewFlowLayout)
        super.init(nibName: nil, bundle: nil)
        self.mainImage = mainImage
        self.smallIcon = smallIcon
        self.style = style
        self.autoclose = autoclose
        self.actions = actions
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var style: ESCAlertStyle = .alert
    
    var smallIcon        : UIImage?
    var mainImage        : UIImage?
    var alertTitle       : String?
    var alertDetail      : String?
    var attributedTitle  : NSAttributedString?
    var attributedDetail : NSAttributedString?
    var keyBoardFrame    : CGRect?
    
    private var spacingBetweenViews: CGFloat                = 16.0
    private var sideOffset: CGFloat                         = 12.0
    private let sectionSpacing: CGFloat                     = 16.0
    private var autoclose: Bool                             = true
    private var separatorHeight: CGFloat                    = 1/UIScreen.main.scale
    private var visibleActionsCountForAlertStyle: CGFloat   = 4.5
    private var isTwoActionsInRow: Bool                     = false
    
    open override var modalPresentationStyle: UIModalPresentationStyle {
        get { .overFullScreen }
        set { }
    }
    
    private var vctd: ViewControllerTransitioningDelegate?
    
    open var containerTapHandler: (() -> Void)? {
        get { vctd?.containerTapHandler }
        set { vctd?.containerTapHandler = newValue }
    }
    
    let smallIconImageView = UIImageView()
    let mainImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = 9
        return imageView
    }()
    
    let titleLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.textAlignment = .center
        titleLabel.numberOfLines = 0
        return titleLabel
    }()
    
    let detailLabel: UILabel = {
        let detailLabel = UILabel()
        detailLabel.textAlignment = .center
        detailLabel.numberOfLines = 0
        return detailLabel
    }()
    
    let contentScrollView = UIScrollView()
    fileprivate let actionsCollectionView: UICollectionView
    fileprivate let actionsCollectionViewFlowLayout: UICollectionViewFlowLayout
    fileprivate let contentView = UIView()
    fileprivate let separatorView = UIView()
    
    private var defaultActions: [ESCAlertAction] = []
    private var cancelActions:  [ESCAlertAction] = []
    
    public var actions: [ESCAlertAction] {
        set {
            defaultActions = newValue.filter({ $0.style == .default })
            cancelActions = newValue.filter({ $0.style == .cancel })
            actionsCollectionView.reloadData()
            view.setNeedsLayout()
        }
        get {
            return defaultActions + cancelActions
        }
    }
    
    private var cellH: CGFloat {
        switch style {
        case .alert: return 44.0
        case .sheet: return 57.0
        }
    }

    open override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(actionsCollectionView)
        if let customView = customView {
            contentView.addSubview(customView)
        } else {
            configureTitleLabel()
            configureDetailLabel()
            configureSmallIconImageView()
            configureMainImageView()
        }
        
        vctd = ViewControllerTransitioningDelegate(configurationScheme: confScheme, style: style)
        transitioningDelegate = vctd
        configureActionsCollectionView()
        configureContentView()
        switch style {
        case .sheet: contentView.backgroundColor = confScheme.backgroundColor
        case .alert: contentView.backgroundColor = .clear
        }
        [view, contentView, contentScrollView].forEach { $0.backgroundColor = confScheme.backgroundColor }
        view.backgroundColor = .clear
        separatorView.backgroundColor = confScheme.separator
        spacingBetweenViews = confScheme.spacingBetweenViews
    }
    
    open override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        pLayout()
    }
    
    private func configureActionsCollectionView() {
        switch style {
        case .alert:
            actionsCollectionView.backgroundColor = confScheme.backgroundColor
            actionsCollectionView.isScrollEnabled = CGFloat(actions.count) > visibleActionsCountForAlertStyle
        case .sheet:
            actionsCollectionView.backgroundColor = .clear
        }
        actionsCollectionView.register(ActionCell.self, forCellWithReuseIdentifier: String(describing: ActionCell.self))
        actionsCollectionView.alwaysBounceVertical = false
        actionsCollectionView.delaysContentTouches = false
        actionsCollectionView.delegate = self
        actionsCollectionView.dataSource = self
        actionsCollectionViewFlowLayout.minimumLineSpacing = separatorHeight
        actionsCollectionViewFlowLayout.minimumInteritemSpacing = separatorHeight
    }
    
    private func configureContentView() {
        switch style {
        case .alert:
            view.addSubview(contentScrollView)
            contentScrollView.addSubview(contentView)
            view.addSubview(separatorView)
        case .sheet:
            actionsCollectionView.addSubview(contentView)
            actionsCollectionView.addSubview(separatorView)
            contentView.clipsToBounds = true
            contentView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            contentView.layer.cornerRadius = confScheme.cornerRadius
        }
    }
    
    private func configureTitleLabel() {
        if let attributedText = attributedTitle {
            titleLabel.attributedText = attributedText
        } else if let text = alertTitle {
            titleLabel.text = text
            titleLabel.textColor = confScheme.titleColor
            switch style {
            case .alert: titleLabel.font = confScheme.titleAlertFont
            case .sheet: titleLabel.font = confScheme.titleSheetFont
            }
        } else {
            return
        }
        contentView.addSubview(titleLabel)
    }
    
    private func configureDetailLabel() {
        if let attributedText = attributedDetail {
            detailLabel.attributedText = attributedText
        } else if let text = alertDetail {
            self.detailLabel.text = text
            detailLabel.textColor = confScheme.messageColor
            
            switch style {
            case .alert: detailLabel.font = confScheme.messageAlertFont
            case .sheet: detailLabel.font = confScheme.messageSheetFont
            }
        } else {
            return
        }
        contentView.addSubview(detailLabel)
    }
    
    private func configureSmallIconImageView() {
        guard smallIcon != nil else { return }
        contentView.addSubview(smallIconImageView)
        smallIconImageView.image = smallIcon
    }
    
    private func configureMainImageView() {
        guard mainImage != nil else { return }
        contentView.addSubview(mainImageView)
        mainImageView.image = mainImage
        mainImageView.contentMode = .scaleAspectFill
    }
    
    private func pContentViewLayouts() {
        _ = configureContentLayoutAndGetHeight()
    }
    
    private func configureContentLayoutAndGetHeight(width: CGFloat? = nil, isNeedSetFrames: Bool = true) -> CGFloat {
        contentView.frame = view.bounds
        
        guard customView == nil else {
            customView?.frame = CGRect(origin: .zero, size: CGSize(width: view.bounds.width, height: customView?.frame.height ?? 0))
            contentView.frame.size.height = customView?.frame.height ?? 0
            return customView?.frame.height ?? 0
        }
        
        var viewWithWhArray: [ViewWithWH] = []
        
        let widthWithoutOffsets = (width != nil) ? width! - 2 * sideOffset : contentView.frame.width - 2 * sideOffset
        
        if smallIcon != nil {
            smallIconImageView.sizeToFit()
            viewWithWhArray.append(ViewWithWH(view: smallIconImageView))
        }
        
        if !(alertTitle == nil && attributedTitle == nil) {
            viewWithWhArray.append(ViewWithWH(view: titleLabel, w: widthWithoutOffsets))
        }
        
        if mainImage != nil {
            viewWithWhArray.append(ViewWithWH(view: mainImageView, w: widthWithoutOffsets))
        }
        
        if !(alertDetail == nil && attributedDetail == nil) {
            viewWithWhArray.append(ViewWithWH(view: detailLabel, w: widthWithoutOffsets))
        }
        
        return self.contentView.configureViewsInStackAndGetHeight(verticalSpacing: spacingBetweenViews, viewWithWhArray: viewWithWhArray, isNeedSetFrames: true)
    }
    
    func getTopContentHeight(forWidth width: CGFloat) -> CGFloat {
        return configureContentLayoutAndGetHeight(width: width, isNeedSetFrames: false)
    }
    
    fileprivate func getAlertHeight(forWidth width: CGFloat, completion: @escaping ((CGFloat)->())) {
        let topContentH = getTopContentHeight(forWidth: width)

        CalculateAlertSizeService.canTwoActionsInRow(alertWidth: width,
                                                     style: self.style,
                                                     actions: self.actions,
                                                     separatorHeight: self.separatorHeight,
                                                     configurationScheme: self.confScheme) { [weak self] isTwoActionsInRow in
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.isTwoActionsInRow = isTwoActionsInRow
                completion(topContentH + self.actionsH + self.separatorHeight)
            }
        }
    }
    
    private var actionsH: CGFloat {
        switch style {
        case .alert:
            let visibleCellsCount = min(CGFloat(actions.count), visibleActionsCountForAlertStyle)
            return isTwoActionsInRow
                ? cellH
                : visibleCellsCount * cellH + max(0,separatorHeight*CGFloat((visibleCellsCount.rounded(.down) - 1)))
        case .sheet:
            return CGFloat(actions.count) * cellH + sectionSpacing + max(0,separatorHeight*CGFloat((actions.count - 1)))
        }
    }
    
    private func pActionViewLayout(contentHeight: CGFloat) {
        actionsCollectionView.frame = view.bounds
        switch style {
        case .alert:
            actionsCollectionView.frame.size.height = actionsH
        case .sheet:
            actionsCollectionView.contentInset.top = contentHeight + separatorHeight
            contentView.frame   = CGRect(x: 0,
                                         y: actionsCollectionView.contentLayoutGuide.layoutFrame.minY - contentHeight - separatorHeight,
                                         width: actionsCollectionView.frame.width,
                                         height: contentHeight)
            separatorView.frame = CGRect(x: 0,
                                         y: actionsCollectionView.contentLayoutGuide.layoutFrame.minY - separatorHeight,
                                         width: actionsCollectionView.frame.width,
                                         height: separatorHeight)
        }
        self.actionsCollectionView.collectionViewLayout.invalidateLayout()
    }
    
    private func pScrollViews() {
        contentScrollView.frame = view.bounds
        contentScrollView.frame.size.height -= (actionsCollectionView.frame.height)
        contentScrollView.contentSize = contentView.frame.size
        contentScrollView.isScrollEnabled = contentScrollView.contentSize.height > contentScrollView.frame.height
        separatorView.frame = CGRect(x: 0, y: contentScrollView.frame.maxY, width: actionsCollectionView.frame.width, height: separatorHeight)
        actionsCollectionView.frame.origin.y = separatorView.frame.maxY.rounded(.down)
    }
    
    private func pLayout() {
        let contentHeight = configureContentLayoutAndGetHeight()
        pActionViewLayout(contentHeight: contentHeight)
        switch style {
        case .alert: pScrollViews()
        case .sheet: break
        }
    }
}

extension ESCAlertViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        switch style {
        case .sheet: return 2
        case .alert: return 1
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch style {
        case .sheet:
            switch section {
            case 0:  return defaultActions.count
            case 1:  return cancelActions.count
            default: return 0
            }
        case .alert:
            return actions.count
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ActionCell.self), for: indexPath) as! ActionCell
        let actions = (style == .sheet) ? (indexPath.section == 0 ? defaultActions : cancelActions) : self.actions
        switch style {
        case .alert:
            cell.titleFont = actions[indexPath.row].style == .default ? confScheme.defaultAlertButtonFont : confScheme.cancelAlertButtonFont
            cell.titleLabel.textColor = actions[indexPath.row].style == .cancel ? confScheme.cancelAlertButtonColor : confScheme.defaultAlertButtonColor
        case .sheet:
            cell.titleFont = actions[indexPath.row].style == .default ? confScheme.defaultSheetButtonFont : confScheme.cancelSheetButtonFont
            cell.titleLabel.textColor = actions[indexPath.row].style == .cancel ? confScheme.cancelSheetButtonColor : confScheme.defaultSheetButtonColor
            cell.backgroundContainerView.layer.cornerRadius = confScheme.cornerRadius
            
            var maskedCorners: CACornerMask = []
            if indexPath.row == 0 {
                if !(indexPath.section == 0 && !contentView.frame.height.isZero) {
                    maskedCorners.insert(.layerMinXMinYCorner)
                    maskedCorners.insert(.layerMaxXMinYCorner)
                }
            }

            if indexPath.row == actions.count - 1 {
                maskedCorners.insert(.layerMinXMaxYCorner)
                maskedCorners.insert(.layerMaxXMaxYCorner)
            }
            cell.backgroundContainerView.layer.maskedCorners = maskedCorners
        }

        cell.titleLabel.text = actions[indexPath.row].title
        cell.configurationScheme = confScheme
        cell.backgroundColor = .clear
        cell.backgroundContainerView.backgroundColor = confScheme.backgroundColor

        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if autoclose {
            DispatchQueue.main.async { self.dismiss(animated: true) }
        }
        switch style {
        case .sheet:
            switch indexPath.section {
            case 0: defaultActions[indexPath.row].block?()
            case 1: cancelActions[indexPath.row].block?()
            default:()
            }
        case .alert:
            actions[indexPath.row].block?()
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView,
                               layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAt indexPath: IndexPath) -> CGSize {
        if isTwoActionsInRow {
            return CGSize(width: ((collectionView.frame.width - separatorHeight)/2).rounded(.down), height: cellH)
        }
        return CGSize(width: collectionView.frame.width, height: cellH)
    }
    
    public func collectionView(_ collectionView: UICollectionView,
                               layout collectionViewLayout: UICollectionViewLayout,
                               insetForSectionAt section: Int) -> UIEdgeInsets {
        switch style {
        case .sheet: return UIEdgeInsets(top: 0, left: 0, bottom: section == 0 ? sectionSpacing : 0, right: 0)
        case .alert: return .zero
        }
    }
}

private class SeparatorLayoutAttributes : UICollectionViewLayoutAttributes {
    var backgroundColor: UIColor?
}

private class ESCAlertCollectionViewFlowLayout: UICollectionViewFlowLayout {
    private var separatorColor: UIColor
    private let separatorDecorationViewKind = String(describing: SeparatorCollectionReusableView.self)
    
    init(separatorColor: UIColor) {
        self.separatorColor = separatorColor
        super.init()
        register(SeparatorCollectionReusableView.self, forDecorationViewOfKind: separatorDecorationViewKind)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        guard let layoutAttributesArray = super.layoutAttributesForElements(in: rect) else { return nil }
        var decorationAttributes: [UICollectionViewLayoutAttributes] = []
        
        for layoutAttributes in layoutAttributesArray {
            let indexPath = layoutAttributes.indexPath
            if let separatorAttributes = layoutAttributesForDecorationView(ofKind: separatorDecorationViewKind, at: indexPath) {
                if rect.intersects(separatorAttributes.frame) {
                    decorationAttributes.append(separatorAttributes)
                }
            }
        }
        return layoutAttributesArray + decorationAttributes
    }
    
    override func layoutAttributesForDecorationView(ofKind elementKind: String, at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        guard let cellAttributes = layoutAttributesForItem(at: indexPath) else {
            return createSeparatorLayoutAttributes(at: indexPath)
        }
        return layoutAttributesForSeparatorView(at: indexPath, for: cellAttributes.frame)
    }
    
    private func createSeparatorLayoutAttributes(at indexPath: IndexPath) -> SeparatorLayoutAttributes {
        let attributes = SeparatorLayoutAttributes(forDecorationViewOfKind: String(describing: SeparatorCollectionReusableView.self), with: indexPath)
        attributes.backgroundColor = separatorColor
        return attributes
    }
    
    private func layoutAttributesForSeparatorView(at indexPath: IndexPath, for cellFrame: CGRect) -> UICollectionViewLayoutAttributes? {
        guard let rect = collectionView?.bounds, indexPath.item > 0 else { return nil }
        let separatorAttributes = createSeparatorLayoutAttributes(at: indexPath)
        if cellFrame.origin.x < cellFrame.width {
            separatorAttributes.frame = CGRect(x: rect.minX,
                                               y: cellFrame.origin.y - minimumLineSpacing,
                                               width: rect.width,
                                               height: minimumLineSpacing)
        } else {
            separatorAttributes.frame = CGRect(x: cellFrame.origin.x - minimumInteritemSpacing,
                                               y: cellFrame.origin.y,
                                               width: minimumInteritemSpacing,
                                               height: cellFrame.height)
        }
        separatorAttributes.zIndex = Int.max
        return separatorAttributes
    }
}

private final class SeparatorCollectionReusableView: UICollectionReusableView {
    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        self.backgroundColor = (layoutAttributes as? SeparatorLayoutAttributes)?.backgroundColor
        self.frame = layoutAttributes.frame
    }
}

fileprivate class ActionCell: UICollectionViewCell {
   
    var titleFont : UIFont!
    var configurationScheme: ESCAlertUIConfigurationScheme?
    let titleLabel = UILabel()
    let backgroundContainerView = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        [backgroundContainerView, titleLabel].forEach({ contentView.addSubview($0) })
        titleLabel.font = titleFont
        titleLabel.textAlignment = .center
        titleLabel.lineBreakMode = .byTruncatingMiddle
        titleLabel.minimumScaleFactor = 0.5
        titleLabel.adjustsFontSizeToFitWidth = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let titleLabelOffset: CGFloat = 7
        backgroundContainerView.frame = contentView.frame
        titleLabel.frame              = CGRect(x: titleLabelOffset,
                                               y: 0,
                                               width: contentView.frame.width - 2 * titleLabelOffset,
                                               height: contentView.frame.height)
    }
    
    override var isHighlighted: Bool {
        didSet {
            guard let configurationScheme = configurationScheme else { return }
            self.backgroundContainerView.backgroundColor = isHighlighted ? configurationScheme.highlightedBackground : configurationScheme.backgroundColor
        }
    }
}

private class ViewControllerTransitioningDelegate: NSObject, UIViewControllerTransitioningDelegate, UIGestureRecognizerDelegate {
    
    fileprivate var configurationScheme: ESCAlertUIConfigurationScheme
    fileprivate var style: ESCAlertStyle
    weak var bottomConstraint: NSLayoutConstraint?
    weak var hConstraint: NSLayoutConstraint?
    weak var containerView: UIView? {
        didSet {
            guard let containerView = containerView else { return }
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureHandler))
            tapGesture.delegate = self
            containerView.addGestureRecognizer(tapGesture)
        }
    }
    weak var presentedVC: UIViewController?
    var alertControllerHeight: CGFloat?
    var containerTapHandler: (()->Void)?
    
    init(configurationScheme: ESCAlertUIConfigurationScheme, style: ESCAlertStyle) {
        self.configurationScheme = configurationScheme
        self.style = style
        super.init()
        if style == .sheet {
            containerTapHandler = { [weak self] in
                self?.presentedVC?.dismiss(animated: true)
            }
        }
        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return touch.view is CustomVisualEffectView
    }
    
    @objc func tapGestureHandler(gesture: UITapGestureRecognizer) {
        containerTapHandler?()
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue,
              let alertHeightWithoutKeyboard = alertControllerHeight,
              let containerView = containerView else { return }
        
        let keyboardFrameMinY = keyboardValue.cgRectValue.minY
        let keyboardVisibleHeight = containerView.bounds.height - keyboardFrameMinY
        let maxAlertHeight = keyboardFrameMinY - containerView.safeAreaInsets.bottom - containerView.safeAreaInsets.top - 20
        let alertNewHeight = keyboardVisibleHeight == 0
            ? alertHeightWithoutKeyboard
            : min(alertHeightWithoutKeyboard, maxAlertHeight)
        
        let bottomConstraintConstant = keyboardVisibleHeight > 0
            ? -(keyboardVisibleHeight + (keyboardFrameMinY - containerView.safeAreaInsets.top - alertNewHeight)/2)
            : -(containerView.bounds.height - alertNewHeight)/2
        
        guard bottomConstraintConstant != self.bottomConstraint?.constant else { return }
        UIView.animate(withDuration: 0.2) {
            self.bottomConstraint?.constant = bottomConstraintConstant
            self.hConstraint?.constant = alertNewHeight
            containerView.layoutIfNeeded()
        }
    }
    
    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        presentedVC = presented
        let presenter = Presenter(configurationScheme: configurationScheme)
        presenter.style = style
        presenter.bottomConstraintUpdateHandler = { [weak self] constraint in
            self?.bottomConstraint = constraint
        }
        presenter.heightConstraintUpdateHandler = { [weak self] constraint in
            self?.hConstraint = constraint
            self?.alertControllerHeight = constraint?.constant
        }
        presenter.containerViewUpdate = { [weak self] view in
            self?.containerView = view
        }
        return presenter
    }

    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let dismisser = Dismisser()
        dismisser.style = style
        dismisser.bottomConstraint = bottomConstraint
        return dismisser
    }
    
    private class CustomVisualEffectView: UIView { }

    private class Presenter: NSObject, UIViewControllerAnimatedTransitioning {
        
        var configurationScheme: ESCAlertUIConfigurationScheme
        init(configurationScheme: ESCAlertUIConfigurationScheme) {
            self.configurationScheme = configurationScheme
        }
        fileprivate var style: ESCAlertStyle = .alert
        fileprivate var bottomConstraintUpdateHandler: ((NSLayoutConstraint?) -> ())?
        fileprivate var heightConstraintUpdateHandler: ((NSLayoutConstraint?) -> ())?
        fileprivate var containerViewUpdate: ((UIView?) -> ())?
        
        var duration: TimeInterval { 0.35 }

        func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval { duration }
        
        func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
            CalculateAlertSizeService.calculateAlertSize(
                containerView: transitionContext.containerView,
                alertVC: transitionContext.viewController(forKey: .to) as! ESCAlertViewController,
                completion: { [weak self] alertSize in
                    self?.animateTransition(transitionContext: transitionContext, alertSize: alertSize)
                })
        }

        func animateTransition(transitionContext: UIViewControllerContextTransitioning, alertSize: CGSize) {
            
            let container = transitionContext.containerView
            let toViewController = transitionContext.viewController(forKey: .to) as! ESCAlertViewController
          
            let toView = toViewController.view!
            toView.layer.masksToBounds = true
            toView.layer.cornerRadius = configurationScheme.cornerRadius
            
            toView.translatesAutoresizingMaskIntoConstraints = false
            
            let efv = CustomVisualEffectView()
            efv.alpha = 0
            efv.backgroundColor = UIColor.black
            efv.frame = UIScreen.main.bounds
            
            [efv, toView].forEach({ container.addSubview($0) })
            
            self.containerViewUpdate?(container)
            
            var bottomConstraint: NSLayoutConstraint
            let hConstraint = toView.heightAnchor.constraint(equalToConstant: alertSize.height)
            
            switch style {
            case .alert:
                bottomConstraint = toView.bottomAnchor.constraint(equalTo: container.bottomAnchor, constant: -(container.frame.height - alertSize.height)/2)
                heightConstraintUpdateHandler?(hConstraint)
                toView.isUserInteractionEnabled = true
                toView.alpha = 0
                toView.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            case .sheet:
                bottomConstraint = toView.bottomAnchor.constraint(equalTo: container.safeAreaLayoutGuide.bottomAnchor, constant: alertSize.height)
                
            }
            bottomConstraintUpdateHandler?(bottomConstraint)
            NSLayoutConstraint.activate([
                bottomConstraint,
                toView.centerXAnchor.constraint(equalTo: container.centerXAnchor),
                toView.widthAnchor.constraint(equalToConstant: alertSize.width),
                hConstraint
            ])
            
            container.layoutIfNeeded()

            let parameters = UICubicTimingParameters(controlPoint1: CGPoint(x: 0.29, y: 0.96), controlPoint2: CGPoint(x: 0.56, y: 0.98))
            let animator = UIViewPropertyAnimator(duration: duration, timingParameters: parameters)
           
            animator.addAnimations {
                efv.alpha = 0.5
                switch self.style {
                case .sheet :
                    bottomConstraint.constant = container.safeAreaInsets.bottom > 0 ? 0 : -8
                case .alert :
                    toView.transform = .identity
                    toView.alpha = 1
                }
                container.layoutIfNeeded()
            }
            
            animator.addCompletion { _ in
                transitionContext.completeTransition(true)
            }
            
            animator.startAnimation()
        }
    }

    private class Dismisser: NSObject, UIViewControllerAnimatedTransitioning {
        var bottomConstraint: NSLayoutConstraint?
        fileprivate var style: ESCAlertStyle = .alert
        var duration: TimeInterval { 0.25 }
        func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval { duration }

        func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {

            let container = transitionContext.containerView
            let efv = container.subviews.first(where: { $0 is CustomVisualEffectView })
            let fromView = transitionContext.view(forKey: .from)!
            container.endEditing(true)

            let parameters = UICubicTimingParameters(controlPoint1: CGPoint(x: 0.29, y: 0.96), controlPoint2: CGPoint(x: 0.56, y: 0.98))
            let animator = UIViewPropertyAnimator(duration: duration, timingParameters: parameters)
            
            animator.addAnimations {
                switch self.style {
                case .sheet :
                    efv?.alpha = 0
                    self.bottomConstraint?.constant = fromView.frame.height + container.safeAreaInsets.bottom
                case .alert :
                    [efv, fromView].forEach({ $0?.alpha = 0 })
                }
                container.layoutIfNeeded()
            }
            
            animator.addCompletion { _ in
                transitionContext.completeTransition(true)
            }
            
            animator.startAnimation()
        }
    }
}



fileprivate class CalculateAlertSizeService {
    private init() {}
    
    static func canTwoActionsInRow(alertWidth: CGFloat,
                                   style: ESCAlertStyle,
                                   actions: [ESCAlertAction],
                                   separatorHeight: CGFloat,
                                   configurationScheme: ESCAlertUIConfigurationScheme,
                                   completion: @escaping ((Bool)->())) {
        DispatchQueue.global(qos: .userInteractive).async {
            guard style == .alert, actions.count == 2 else {
                completion(false)
                return
            }
            
            let indexes = actions[0].title.count > actions[1].title.count ? [0, 1] : [1, 0]
            let wOffset: CGFloat = 10
            let maxTitleWidth: CGFloat = ((alertWidth - separatorHeight)/2) - 2 * wOffset
            
            for index in indexes {
                let titleFont = actions[index].style == .default ? configurationScheme.defaultAlertButtonFont : configurationScheme.cancelAlertButtonFont
                let actionWidth = actions[index].title.size(withAttributes: [NSAttributedString.Key.font: titleFont]).width
                if actionWidth > maxTitleWidth {
                    completion(false)
                    return
                }
            }
            completion(true)
        }
    }
    
    static func calculateAlertSize(containerView: UIView, alertVC: ESCAlertViewController, completion: @escaping ((CGSize)->())) {
       
        let maxH: CGFloat = containerView.safeAreaLayoutGuide.layoutFrame.height - 28
        let width: CGFloat
        
        switch alertVC.style {
        case .alert: width = 270
        case .sheet:
            let offset: CGFloat = 8.0
            width = UIDevice.current.userInterfaceIdiom == .phone ? containerView.frame.width - 2 * offset : 304
        }
        
        alertVC.getAlertHeight(forWidth: width, completion: { height in
            completion(CGSize(width: width, height: min(height, maxH)))
        })
    }
}
