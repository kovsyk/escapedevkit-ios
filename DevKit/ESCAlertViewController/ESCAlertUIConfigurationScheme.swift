//
//  ESCAlertUIConfigurationScheme.swift
//  EscDevKit
//
//  Created by EscapeTech on 09.03.2021.
//  Copyright © 2021 EscapeTech. All rights reserved.
//

import Foundation
import UIKit

public struct ESCAlertUIConfigurationScheme {
    
    public var titleColor: UIColor
    public var messageColor: UIColor
    public var backgroundColor: UIColor
    public var highlightedBackground: UIColor
    public var defaultSheetButtonColor: UIColor
    public var cancelSheetButtonColor: UIColor
    public var defaultAlertButtonColor: UIColor
    public var cancelAlertButtonColor: UIColor
    public var separator: UIColor
    public var cornerRadius: CGFloat
    public var defaultSheetButtonFont: UIFont
    public var cancelSheetButtonFont: UIFont
    public var defaultAlertButtonFont: UIFont
    public var cancelAlertButtonFont: UIFont
    public var titleSheetFont: UIFont
    public var messageSheetFont: UIFont
    public var titleAlertFont: UIFont
    public var messageAlertFont: UIFont
    public var spacingBetweenViews: CGFloat
    
    public static var defaultScheme = ESCAlertUIConfigurationScheme(
        titleColor:              ESCAlertSystemColors.label,
        messageColor:            ESCAlertSystemColors.secondaryLabel,
        backgroundColor:         ESCAlertSystemColors.tertiarySystemBackground,
        highlightedBackground:   ESCAlertSystemColors.secondarySystemBackground,
        defaultSheetButtonColor: .systemBlue,
        cancelSheetButtonColor:  .systemRed,
        defaultAlertButtonColor: .systemBlue,
        cancelAlertButtonColor:  .systemRed,
        separator:               ESCAlertSystemColors.separator,
        cornerRadius:            16.0,
        defaultSheetButtonFont:  UIFont.systemFont(ofSize: 20, weight: .regular),
        cancelSheetButtonFont:   UIFont.systemFont(ofSize: 20, weight: .bold),
        defaultAlertButtonFont:  UIFont.systemFont(ofSize: 16, weight: .regular),
        cancelAlertButtonFont:   UIFont.systemFont(ofSize: 16, weight: .bold),
        titleSheetFont:          UIFont.systemFont(ofSize: 16, weight: .medium),
        messageSheetFont:        UIFont.systemFont(ofSize: 14, weight: .regular),
        titleAlertFont:          UIFont.systemFont(ofSize: 14, weight: .medium),
        messageAlertFont:        UIFont.systemFont(ofSize: 12, weight: .regular),
        spacingBetweenViews:     16.0)
}

struct ESCAlertSystemColors {
    static var label: UIColor {
        if #available(iOS 13, *) {
            return .label
        } else {
            return UIColor(red: 0, green: 0, blue: 0, alpha: 1.0)
        }
    }
    
    static var secondaryLabel: UIColor {
        if #available(iOS 13, *) {
            return .secondaryLabel
        } else {
            return UIColor(red: CGFloat(60.0/255.0), green: CGFloat(60.0/255.0), blue: CGFloat(67.0/255.0), alpha: CGFloat(0.6))
        }
    }
    
    static var tertiarySystemBackground: UIColor {
        if #available(iOS 13, *) {
            return .tertiarySystemBackground
        } else {
            return UIColor(red: CGFloat(255.0/255.0), green: CGFloat(255.0/255.0), blue: CGFloat(255.0/255.0), alpha: CGFloat(1.0))
        }
    }
    
    static var secondarySystemBackground: UIColor {
        if #available(iOS 13, *) {
            return .secondarySystemBackground
        } else {
            return UIColor(red: CGFloat(242.0/255.0), green: CGFloat(242.0/255.0), blue: CGFloat(247.0/255.0), alpha: CGFloat(1.0))
        }
    }
    static var separator: UIColor {
        if #available(iOS 13, *) {
            return .opaqueSeparator
        } else {
            return UIColor(red: CGFloat(198.0/255.0), green: CGFloat(198.0/255.0), blue: CGFloat(200.0/255.0), alpha: CGFloat(1.0))
        }
    }
}
