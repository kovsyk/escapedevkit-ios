//
//  ViewController.swift
//  EscDevKit
//
//  Created by EscapeTech on 11.06.2020.
//  Copyright © 2020 EscapeTech. All rights reserved.
//

import UIKit
import Photos

open class ESCAvatarSelectionViewController: UIViewController {
    var image: UIImage?
    var asset: PHAsset?    
    var completionBlock: ((_ fullPhoto: UIImage, _ avatarPhoto: UIImage, _ avatarPosition: CGRect)->())?
    var configurationScheme: ESCAvatarSelectionUIConfigurationScheme?
    var strings: ESCPhotoModuleStrings = ESCPhotoModuleStrings.defaultStrings
    let toolbarHeight: CGFloat = 44
    
    lazy var toolbar = UIToolbar(frame: CGRect(x: 0, y: view.frame.height - toolbarHeight, width: view.bounds.width, height: toolbarHeight))
    var cancelButton: UIBarButtonItem?
    var usePhotoButton: UIBarButtonItem?
    
    var avatarImageView = UIImageView()
    var frameView: ESCFrameView!
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        view.clipsToBounds = true
        view.backgroundColor = ESCAlertSystemColors.tertiarySystemBackground
        view.addSubview(avatarImageView)
        avatarImageView.image = image
        configureFrameView()
        configureToolBar()
        configureUiWithScheme()
        view.addSubview(toolbar)
    }
    
    open override func viewSafeAreaInsetsDidChange() {
        super.viewSafeAreaInsetsDidChange()
        updateFrames()
    }

    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateFrames()
    }
    
    open override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateImageWithMaxSize()
    }
    
    func configureToolBar() {
        cancelButton = UIBarButtonItem(title: strings.cancel, style: .plain, target: self, action: #selector(close))
        usePhotoButton = UIBarButtonItem(title: strings.usePhoto, style: .plain, target: self, action: #selector(confirmPhoto))
        
        toolbar.items = [
            cancelButton!,
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            usePhotoButton!
        ]
    }
    
    func configureFrameView() {
        var escFrameView = ESCFrameView()
        
        if let confScheme = configurationScheme {
            switch confScheme.frameViewStyle{
            case .square:
                escFrameView = ESCFrameView(frame: view.frame)
            case .circle:
                escFrameView = ESCOvalView(frame: view.frame)
            }
            escFrameView.lineWidth = confScheme.frameViewLineWidth
            escFrameView.strokeColor = confScheme.frameViewStrokeColor
        } else {
            escFrameView = ESCOvalView(frame: view.frame)
            escFrameView.lineWidth = 0
            escFrameView.strokeColor = .clear
        }
        if !view.subviews.contains(escFrameView) {
            view.addSubview(escFrameView)
        }
        frameView = escFrameView
        let side = min(view.frame.width, view.frame.height)
        frameView.frameSize = CGSize(width: side, height: side)
        frameView?.isMultipleTouchEnabled = true
    }
    
    func configureUiWithScheme() {
        guard let scheme = configurationScheme else {
            return
        }
        
        cancelButton?.setTitleTextAttributes([
                            .font: scheme.toolBarButtonsFont,
                            .foregroundColor: UIColor.white], for: .normal)
        
        usePhotoButton?.setTitleTextAttributes([
                            .font: scheme.toolBarButtonsFont,
                            .foregroundColor: scheme.toolBarButtonsColor], for: .normal)
        
        frameView.fillColor = scheme.frameViewFillColor.withAlphaComponent(scheme.frameViewFillOpacity)
        toolbar.barTintColor = scheme.toolBarColor
        toolbar.alpha = scheme.toolBarOpacity
    }
    
    func updateFrames() {
        toolbar.frame.size.width = view.frame.width
        toolbar.frame.size.height = toolbarHeight
        toolbar.frame.origin.y = view.frame.height - toolbar.frame.size.height - view.safeAreaInsets.bottom
        frameView.frame = view.frame
        fixAvatarPosition()
    }
    
    private func updateImageWithMaxSize() {
        guard let asset = asset else { return }
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return  }
            let requestOptions = PHImageRequestOptions()
            requestOptions.isSynchronous = true
            PHImageManager.default().requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFill, options: requestOptions, resultHandler: { [weak self] (image, _) in
                if let image = image {
                    DispatchQueue.main.async {
                        self?.avatarImageView.image = image
                        self?.updateFrames()
                    }
                }
            })
        }
    }
    
    @objc func confirmPhoto() {
        
        func cropImage(image: UIImage, rect: CGRect) -> UIImage? {
            if let cgImage = image.cgImage, let croppedCGImage = cgImage.cropping(to: rect) {
                return UIImage(cgImage: croppedCGImage)
            }
            return nil
        }
        
        var avatarPositionFrame = frameView.openedFrame
        avatarPositionFrame.origin.x -= avatarImageView.frame.origin.x
        avatarPositionFrame.origin.y -= avatarImageView.frame.origin.y

        avatarPositionFrame.origin.x = avatarPositionFrame.origin.x / avatarImageView.frame.width
        avatarPositionFrame.origin.y = avatarPositionFrame.origin.y / avatarImageView.frame.height
        
        avatarPositionFrame.size.width = avatarPositionFrame.size.width / avatarImageView.frame.width
        avatarPositionFrame.size.height = avatarPositionFrame.size.height / avatarImageView.frame.height
        if let image = avatarImageView.image?.fixedOrientation() {
            if let avatarImage = cropImage(
                    image: image,
                    rect: CGRect(
                        x: avatarPositionFrame.origin.x * image.size.width,
                        y: avatarPositionFrame.origin.y * image.size.height,
                        width: avatarPositionFrame.size.width * image.size.width,
                        height: avatarPositionFrame.size.height * image.size.height
                    )
                ) {
                completionBlock?(image, avatarImage, avatarPositionFrame)
                close()
            }
        }
    }
    
    @objc func close() {
        dismiss(animated: true, completion: nil)
    }

    private func fixAvatarPosition() {
        avatarImageView.sizeToFit()
        
        var avatarFrame: CGRect = avatarImageView.frame
        
        var scale: CGFloat = 1
        if  avatarImageView.frame.size.width > avatarImageView.frame.size.height {
            scale = avatarImageView.frame.size.width / frameView.frameSize.width
        }
        else {
            scale = avatarImageView.frame.size.height / frameView.frameSize.height
        }

        if scale > 1 {
            avatarFrame.size.width *= 1/scale
            avatarFrame.size.height *= 1/scale
        }
        avatarFrame.origin.x = frameView.center.x - avatarFrame.width/2
        avatarFrame.origin.y = frameView.center.y - avatarFrame.height/2

        avatarImageView.frame = calculateFrame(avatarFrame)
    }
    
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        navigationItem.rightBarButtonItem?.isEnabled = false
    }
    
    open override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        guard allTouchesContainsIn(view: frameView, touches: touches) else {
            animateFrameRecalculation()
            return
        }

        if touches.count == 1, let touch = touches.first {
            let previousLocation = touch.previousLocation(in: avatarImageView)
            let currentLocation = touch.location(in: avatarImageView)
            avatarImageView.center = CGPoint(
                x: avatarImageView.center.x + currentLocation.x - previousLocation.x,
                y: avatarImageView.center.y + currentLocation.y - previousLocation.y
            )
        }
        else if touches.count == 2 {
            let touches = Array(touches)
            if let touch1 = touches.first, let touch2 = touches.last {
                let previousLocation1   = touch1.previousLocation(in: avatarImageView)
                let currentLocation1    = touch1.location(in: avatarImageView)
                let previousLocation2   = touch2.previousLocation(in: avatarImageView)
                let currentLocation2    = touch2.location(in: avatarImageView)
                
                let previous = hypotf(Float(previousLocation1.x - previousLocation2.x), Float(previousLocation1.y - previousLocation2.y))
                let current = hypotf(Float(currentLocation1.x - currentLocation2.x), Float(currentLocation1.y - currentLocation2.y))
                
                let scale = CGFloat(current/previous)
                
                avatarImageView.center = CGPoint(
                    x: avatarImageView.center.x + (currentLocation1.x - previousLocation1.x)/2 + (currentLocation2.x - previousLocation2.x)/2,
                    y: avatarImageView.center.y + (currentLocation1.y - previousLocation1.y)/2 + (currentLocation2.y - previousLocation2.y)/2
                )
                
                let center = avatarImageView.center
                let size = avatarImageView.frame.size
                avatarImageView.frame.size.width *= scale
                avatarImageView.frame.size.height *= scale
                avatarImageView.center = center
                
                let xPadding = (1 - scale) * ((currentLocation1.x + currentLocation2.x) / (2 * size.width) - 0.5) * size.width
                let yPadding = (1 - scale) * ((currentLocation1.y + currentLocation2.y) / (2 * size.height) - 0.5) * size.height
                
                avatarImageView.center = CGPoint(
                    x: center.x + xPadding,
                    y: center.y + yPadding
                )
            }
        }
    }
    
    open override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        animateFrameRecalculation()
        navigationItem.rightBarButtonItem?.isEnabled = true
    }
    
    open override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        animateFrameRecalculation()
        navigationItem.rightBarButtonItem?.isEnabled = true
    }
    
    private func animateFrameRecalculation() {
        let frame = calculateFrame(avatarImageView.frame)
        if frame.size == .zero {
            view.isUserInteractionEnabled = false
            UIView.animate(withDuration: 0.2) {
                self.view.isUserInteractionEnabled = true
                self.fixAvatarPosition()
            }
        }
        else {
            view.isUserInteractionEnabled = false
            UIView.animate(withDuration: 0.2) {
                self.view.isUserInteractionEnabled = true
                self.avatarImageView.frame = frame
            }
        }
    }
    
    private func calculateFrame(_ avatarFrame: CGRect) -> CGRect {
        if let openedFrame = frameView?.openedFrame, let intersection = frameView?.openedFrame.intersection(avatarFrame) {
            guard intersection.size != .zero else { return .zero }
            var compensation = CGRect.zero
            compensation.origin.x       = intersection.origin.x     - openedFrame.origin.x
            compensation.origin.y       = intersection.origin.y     - openedFrame.origin.y
            compensation.size.width     = openedFrame.size.width    - intersection.size.width
            compensation.size.height    = openedFrame.size.height   - intersection.size.height
            
            if compensation != .zero {
                var scale: CGFloat = 1
                var avatarFrame = avatarFrame
                if compensation.origin.x > 0 {
                    avatarFrame.origin.x -= compensation.origin.x
                    compensation.size.width -= compensation.origin.x
                }
                if compensation.size.width > 0 {
                    if compensation.origin.x > 0 {
                        scale = (frameView!.frameSize.width)/avatarFrame.width
                    }
                    else {
                        avatarFrame.origin.x += compensation.size.width
                    }
                    
                }
                if compensation.origin.y > 0 {
                    avatarFrame.origin.y -= compensation.origin.y
                    compensation.size.height -= compensation.origin.y
                }
                if compensation.size.height > 0 {
                    if compensation.origin.y > 0 {
                        scale = max(scale, frameView!.frameSize.height/avatarFrame.height)
                    }
                    else {
                        avatarFrame.origin.y += compensation.size.height
                    }
                }
                
                if scale != 1 {
                    avatarFrame.size.width *= scale
                    avatarFrame.size.height *= scale

                    avatarFrame.origin = CGPoint(
                        x: view.frame.width/2 - avatarFrame.width/2,
                        y: view.frame.height/2 - avatarFrame.height/2
                    )
                    return calculateFrame(avatarFrame)
                }
                else { return avatarFrame }
            }
            else { return avatarFrame }
        }
        else { return avatarFrame }
    }
    
    private func allTouchesContainsIn(view: UIView, touches: Set<UITouch>) -> Bool {
        var containsInView = true
        touches.forEach({ (touch) in
            let point = touch.location(in: view)
            containsInView = containsInView && view.frame.contains(point)
        })
        return containsInView
    }
}

private extension UIImage {
    func fixedOrientation() -> UIImage {
        if imageOrientation == .up { return self }
        
        let cgImage = self.cgImage!
        
        var transform: CGAffineTransform = .identity
        
        switch imageOrientation {
        case .down, .downMirrored:
            transform = transform.translatedBy(x: size.width, y: size.height)
            transform = transform.rotated(by: CGFloat.pi)
            break
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.rotated(by: CGFloat.pi / 2.0)
            break
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: size.height)
            transform = transform.rotated(by: CGFloat.pi / -2.0)
            break
            
        case .up, .upMirrored:
            break
        @unknown default:
            break
        }
        
        switch imageOrientation {
        case .upMirrored, .downMirrored:
            transform.translatedBy(x: size.width, y: 0)
            transform.scaledBy(x: -1, y: 1)
            break
        case .leftMirrored, .rightMirrored:
            transform.translatedBy(x: size.height, y: 0)
            transform.scaledBy(x: -1, y: 1)
        case .up, .down, .left, .right:
            break
        @unknown default:
            break
        }
        
        let context: CGContext = CGContext(data: nil,
                                           width: Int(size.width),
                                           height: Int(size.height),
                                           bitsPerComponent: cgImage.bitsPerComponent,
                                           bytesPerRow: 0,
                                           space: cgImage.colorSpace!,
                                           bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!
        
        context.concatenate(transform)
        switch imageOrientation {
        case .left, .leftMirrored, .right, .rightMirrored:
            context.draw(cgImage,
                         in: CGRect(x: 0, y: 0, width: size.height, height: size.width))
        default:
            context.draw(cgImage,
                         in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            break
        }
        
        return UIImage(cgImage: context.makeImage()!)
    }
}
