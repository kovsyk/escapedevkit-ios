//
//  AvatarModule.swift
//  EscDevKit
//
//  Created by EscapeTech on 11.06.2020.
//  Copyright © 2020 EscapeTech. All rights reserved.
//

import Foundation
import Photos
import UIKit
import AVFoundation
import PhotosUI

enum PhotoModuleMode {
    case image, avatar
}

public struct ESCPhotoModuleStrings {
    public var camera                      : String
    public var photos                      : String
    public var allowAccess                 : String
    public var photoGalleryRequestMessage  : String
    public var photoCameraRequestMessage   : String
    public var openSettings                : String
    public var cancel                      : String
    public var usePhoto                    : String
    
    public static var defaultStrings = ESCPhotoModuleStrings(
        camera:                     "Camera",
        photos:                     "Photos",
        allowAccess:                "Allow Access",
        photoGalleryRequestMessage: "To select a photo, the application needs access to the photo gallery",
        photoCameraRequestMessage:  "To take a photo, the application needs access to the camera",
        openSettings:               "Open Settings",
        cancel:                     "Cancel",
        usePhoto:                   "Use Photo"
    )
}

public struct ESCAvatarSelectionUIConfigurationScheme {
   
    public var frameViewStyle: ESCFrameViewStyle
    public var toolBarColor: UIColor?
    public var toolBarOpacity: CGFloat
    public var toolBarButtonsColor: UIColor
    public var toolBarButtonsFont: UIFont
    public var frameViewLineWidth: CGFloat
    public var frameViewStrokeColor: UIColor
    public var frameViewFillColor: UIColor
    public var frameViewFillOpacity: CGFloat
    public var cameraIcon: UIImage?
    public var cameraCellBackColor: UIColor
    
    public static var defaultScheme = ESCAvatarSelectionUIConfigurationScheme(
        frameViewStyle: .circle,
        toolBarColor: ESCAlertSystemColors.label,
        toolBarOpacity: 0.8,
        toolBarButtonsColor: ESCAlertSystemColors.tertiarySystemBackground,
        toolBarButtonsFont: UIFont.systemFont(ofSize: 17),
        frameViewLineWidth: 0,
        frameViewStrokeColor: .black,
        frameViewFillColor: .black,
        frameViewFillOpacity: 0.6,
        cameraIcon: nil,
        cameraCellBackColor: .lightGray)
}

public struct ESCPhotoSelectorUIConfigurationScheme {
    public static var defaultScheme = ESCPhotoSelectorUIConfigurationScheme(avatarSelectionUIConfScheme: ESCAvatarSelectionUIConfigurationScheme.defaultScheme)
    
    var alertUIConfScheme: ESCAlertUIConfigurationScheme?
    public var avatarSelectionUIConfScheme: ESCAvatarSelectionUIConfigurationScheme?
}

public class ESCPhotoSelector: ESCMediaSelector {
    
    var mode: PhotoModuleMode = .image
    var avatarSelectionViewControllerType: ESCAvatarSelectionViewController.Type = ESCAvatarSelectionViewController.self
    var configurationScheme: ESCPhotoSelectorUIConfigurationScheme?
    var avatarResultingHandler: ((_ fullPhoto: UIImage, _ avatarPhoto: UIImage, _ avatarPosition: CGRect)->())?
    var photoResultingHandler: ((UIImage?)->Void)?
    weak var presentingViewController: UIViewController?
    fileprivate weak var photoContainer: PhotoContainer?
    var strings: ESCPhotoModuleStrings = ESCPhotoModuleStrings.defaultStrings

    open class func getImage(from presentingViewController: UIViewController,
                             withFastAccessView: Bool = false,
                             isWritePhotoToAlbum: Bool = false,
                             cameraPermissionDeniedHandler: (()->Void)? = nil,
                             alertUIConfScheme: ESCAlertUIConfigurationScheme = .defaultScheme,
                             errorHandler: ((Error)-> Void)? = nil,
                             imageCompletion: @escaping (UIImage?)->()) -> ESCPhotoSelector {
        let module = self.init(isWritePhotoToAlbum: isWritePhotoToAlbum,
                               errorHandler: errorHandler)
        module.mode = .image
        module.configurationScheme = ESCPhotoSelectorUIConfigurationScheme(alertUIConfScheme: alertUIConfScheme)
        module.presentingViewController = presentingViewController
        module.photoResultingHandler = imageCompletion
        if cameraPermissionDeniedHandler == nil { module.configureCameraPermissionDenied() }
        module.requestMedia(presentingViewController: presentingViewController, withFastAccessView: withFastAccessView)
        return module
    }
    
    open class func getAvatar(from presentingViewController: UIViewController,
                              withFastAccessView: Bool = false,
                              isWritePhotoToAlbum: Bool = false,
                              cameraPermissionDeniedHandler: (()->Void)? = nil,
                              configurationScheme: ESCPhotoSelectorUIConfigurationScheme = .defaultScheme,
                              errorHandler: ((Error)-> Void)? = nil,
                              avatarSelectionViewControllerType: ESCAvatarSelectionViewController.Type = ESCAvatarSelectionViewController.self,
                              imageCompletion: @escaping (_ fullPhoto: UIImage, _ avatarPhoto: UIImage, _ avatarPosition: CGRect)->()) -> ESCPhotoSelector {
        let module = self.init(isWritePhotoToAlbum: isWritePhotoToAlbum,
                               errorHandler: errorHandler)
        module.mode = .avatar
        module.configurationScheme = configurationScheme
        module.presentingViewController = presentingViewController
        module.avatarSelectionViewControllerType = avatarSelectionViewControllerType
        module.configureCameraPermissionDenied()
        module.avatarResultingHandler = imageCompletion
        if cameraPermissionDeniedHandler == nil { module.configureCameraPermissionDenied() }
        module.pickerDelegate = ESCPhotoPickerDelegate(mediaSelector: module, directoryForSaving: nil)
        module.requestMedia(presentingViewController: presentingViewController, withFastAccessView: withFastAccessView)
        return module
    }
    
    func requestMedia(presentingViewController: UIViewController, withFastAccessView: Bool = false) {
        
        func showRequestSheet(withCamera: Bool, withLibrary: Bool) {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                let controller = ESCAlertViewController(
                    style: .sheet,
                    autoclose: false,
                    actions: [],
                    configurationScheme: self.configurationScheme?.alertUIConfScheme ?? ESCAlertUIConfigurationScheme.defaultScheme)
                weak var _controller = controller
                let actions: [ESCAlertAction] = [
                    ESCAlertAction(title: self.strings.camera, style: .default,    block: { [weak self] in
                        _controller?.dismiss(animated: true) {
                            self?.showPickerController(presentingViewController: presentingViewController, mediaType: .image, sourceType: .camera)
                        }
                    }),
                    ESCAlertAction(title: self.strings.photos, style: .default,    block: { [weak self] in
                        _controller?.dismiss(animated: true) {
                            self?.showPickerController(presentingViewController: presentingViewController, mediaType: .image, sourceType: .photoLibrary(selectionCount: 1))
                        }
                    }),
                    ESCAlertAction(title: self.strings.cancel, style: .cancel,     block: {
                        _controller?.dismiss(animated: true)
                    })]
                controller.actions = actions
                if withLibrary {
                    let view = PhotoContainer(frame: CGRect(origin: .zero, size: CGSize(width: 100, height: 112)))
                    view.cameraIcon = self.configurationScheme?.avatarSelectionUIConfScheme?.cameraIcon
                    view.cameraCellBackColor = self.configurationScheme?.avatarSelectionUIConfScheme?.cameraCellBackColor
                    view.mediaSelector = self
                    controller.customView = view
                    weak var _controller = controller
                    view.selectedImageHandler = { [weak self] photo, asset in
                        _controller?.dismiss(animated: true) {
                            self?.processImageResult(asset: asset, image: photo, directoryForSaving: nil)
                        }
                    }
                    if withCamera {
                        view.cameraHandler = { [weak self] in
                            _controller?.dismiss(animated: true) {
                                self?.showImageViewPickerController(presentingViewController: presentingViewController, mediaType: .image, sourceType: .camera)
                            }
                        }
                    }

                    self.photoContainer = view
                    self.mediaFetchModule.getFetchResults(mediaTypes: [.image], onFetchResultUpdated: { [weak self] photoFetchResult in
                        self?.photoContainer?.previewIcons = [UIImage?].init(repeating: nil, count: photoFetchResult.count)
                        self?.photoContainer?.collectionView?.reloadData()
                    })
                }
                
                presentingViewController.present(controller, animated: true)
            }
        }

        if withFastAccessView {
            checkLibraryPermission(statusHandler: { [weak self] isDenied in
                guard let self = self else { return }
                if isDenied {
                    showRequestSheet(withCamera: false, withLibrary: false)
                } else {
                    self.checkCameraPermission(statusHandler: { isCameraDenied in
                        if isCameraDenied {
                            showRequestSheet(withCamera: false, withLibrary: true)
                        } else {
                            showRequestSheet(withCamera: true, withLibrary: true)
                        }
                    })
                }
            })
        }
        else {
            showRequestSheet(withCamera: false, withLibrary: false)
        }
    }
    
    func configureCameraPermissionDenied() {
        cameraPermissionDeniedHandler = { [weak self] in
            guard let self = self else { return }
            self.showAlert(presentingViewController: self.presentingViewController,
                           title: self.strings.allowAccess,
                           message: self.strings.photoCameraRequestMessage,
                           okTitle: self.strings.openSettings,
                           okHandler: {
                            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, completionHandler: nil)
                            return
                           }, cancelHandler: { (action) in
                            return
                           })
        }
    }
    
    override func processImageResult(asset: PHAsset?, image: UIImage, directoryForSaving: URL?) {
        switch mode {
        case .image:
            if let asset = asset {
                let requestOptions = PHImageRequestOptions()
                requestOptions.isSynchronous = true
                mediaFetchModule.requestImage(asset: asset, size: PHImageManagerMaximumSize, requestOptions: requestOptions){ [weak self] (image) in
                    DispatchQueue.main.async { [weak self] in
                        self?.photoResultingHandler?(image)
                    }
                }
            }
            else {
                DispatchQueue.main.async { [weak self] in
                    self?.photoResultingHandler?(image)
                }
            }
        case .avatar:
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                let avatarViewController: ESCAvatarSelectionViewController = self.avatarSelectionViewControllerType.init()
                avatarViewController.configurationScheme = self.configurationScheme?.avatarSelectionUIConfScheme
                avatarViewController.image = image
                avatarViewController.asset = asset
                avatarViewController.completionBlock = self.avatarResultingHandler
                avatarViewController.strings = self.strings
                avatarViewController.modalPresentationStyle = .overFullScreen
                self.present(vc: avatarViewController, presentingViewController: self.presentingViewController)
            }
        }
    }
}

class ESCPhotoPickerDelegate: ESCMediaPickerDelegate {
    @available(iOS 14.0, *)
    public override func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
        picker.dismiss(animated: true)
        guard let firstResult = results.first else {
            mediaSelector?.errorHandler?(ESCMediaSelectorError.unknownError)
            return
        }
        
        if firstResult.itemProvider.canLoadObject(ofClass: UIImage.self) {
            firstResult.itemProvider.loadObject(ofClass: UIImage.self, completionHandler: { [weak self] (image, error) in
                guard let image = image as? UIImage else {
                    self?.mediaSelector?.errorHandler?(error ?? ESCMediaSelectorError.unknownError)
                    return
                }
                self?.mediaSelector?.processImageResult(asset: nil, image: image, directoryForSaving: self?.directoryForSaving)
            })
        }
    }
}

fileprivate class PhotoContainer: HorizontalContainerPhotoModule {
    
    var selectedImageHandler: ((_ preview: UIImage, _ asset: PHAsset)->())?
    var cameraHandler: (()->())?
    var cameraIcon: UIImage?
    var cameraCellBackColor: UIColor?
    weak var mediaSelector: ESCMediaSelector?
    var addIsAvailable: Bool { cameraHandler != nil }
    var photoSide: CGFloat = 104.0
    override var cellClass: UICollectionViewCell.Type { PhotoCell.self }
    var previewIcons: [UIImage?] = []
    private let addPhotoCellId = "AddPhotoCell"
    
    override func pCollectionView() {
        super.pCollectionView()
        collectionView?.backgroundColor = .clear
        collectionView?.register(AddPhotoCell.self, forCellWithReuseIdentifier: addPhotoCellId)
        collectionView?.allowsSelection = true
        collectionView?.contentInset.left += 6
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int { 2 }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 { return addIsAvailable ? 1 : 0 }
        return previewIcons.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: photoSide, height: photoSide)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: addPhotoCellId, for: indexPath) as! AddPhotoCell
            cell.iconView.image = cameraIcon
            cell.backView.backgroundColor = cameraCellBackColor
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! PhotoCell
        if let previewImage = previewIcons[indexPath.row] {
            cell.photoImageView.image = previewImage
        }
        else {
            cell.photoImageView.contentMode = .scaleAspectFill
            cell.background.backgroundColor = .clear
            let requestOptions = PHImageRequestOptions()
            requestOptions.isSynchronous = true
            requestOptions.resizeMode = .fast
            cell.photoImageView.image = nil
            mediaSelector?.mediaFetchModule.requestFetchResultImage(index: indexPath.row,
                                        size: CGSize(width: photoSide * UIScreen.main.scale,
                                                     height: photoSide * UIScreen.main.scale),
                                        requestOptions: requestOptions) { (image) in
                if let image = image {
                    DispatchQueue.main.async { [weak self] in
                        self?.previewIcons[indexPath.row] = image
                        self?.collectionView?.reloadItems(at: [indexPath])
                    }
                }
            }
        }
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: addPhotoCellId, for: indexPath) as! AddPhotoCell
            cell.session?.stopRunning()
            cameraHandler?()
        }
        else {
            let image = previewIcons[indexPath.row] ?? UIImage()
            selectedImageHandler?(image, mediaSelector?.mediaFetchModule.getFetchResultAsset(index: indexPath.row) ?? PHAsset())
        }
    }
    
    var photoOffset: CGFloat = 8
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat { photoOffset }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        UIEdgeInsets(top: 0, left: 0, bottom: 0, right: photoOffset)
    }
}

class HorizontalContainerPhotoModule: UIView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    let cellId = "CellId"
    var cellClass: UICollectionViewCell.Type { UICollectionViewCell.self }
    var collectionView: UICollectionView?
    
    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if let panGR = gestureRecognizer as? UIPanGestureRecognizer {
            let velocity = panGR.velocity(in: collectionView)
            return abs(velocity.y) > abs(velocity.x)
        }
        return super.gestureRecognizerShouldBegin(gestureRecognizer)
    }
    
    fileprivate func pCollectionView() {
        if #available(iOS 11.0, *) {
            collectionView?.contentInsetAdjustmentBehavior = .never
        }

        collectionView?.delegate    = self
        collectionView?.dataSource  = self
        collectionView?.register(cellClass, forCellWithReuseIdentifier: cellId)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        collectionView?.frame  = bounds
        collectionView?.center = CGPoint(x: bounds.width/2, y: bounds.height/2)
    }
    
    override func didMoveToSuperview() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        collectionView = UICollectionView(frame: bounds,
                                               collectionViewLayout: layout)
        
        collectionView?.showsHorizontalScrollIndicator = false
        collectionView?.showsVerticalScrollIndicator   = false
        
        if #available(iOS 10.0, *) {
            collectionView?.isPrefetchingEnabled = false
        }
        pCollectionView()
        addSubview(collectionView!)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int { 1 }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int { 0 }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) { }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) { }
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) { }
    func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool { return true }
    func scrollViewDidScroll(_ scrollView: UIScrollView) { }
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) { }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) { }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) { }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat { 0 }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat { 0 }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         CGSize(width: bounds.height, height: bounds.height)
    }
}

private class PhotoCell: UICollectionViewCell {
    
    var photoImageView      = UIImageView()
    var background          = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        prepare()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        prepare()
    }
    
    func prepare() {
        contentView.addSubview(background)
        contentView.addSubview(photoImageView)
        contentView.backgroundColor = .clear
        background.layer.cornerRadius = 12
        photoImageView.layer.cornerRadius = 12
        photoImageView.layer.masksToBounds = true
        photoImageView.contentMode = .scaleAspectFill
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        background.frame = bounds
        photoImageView.frame = CGRect(
            x: 1,
            y: 1,
            width: bounds.width-2,
            height: bounds.height-2
        )
    }
}

private class AddPhotoCell: UICollectionViewCell {
    
    let icon = UIImage(named: "PhotoIcon")
    
    deinit {
        session?.stopRunning()
    }
    
    var session: AVCaptureSession?
    
    var videoOutput: AVCaptureVideoDataOutput?
    let videoDataOutput = AVCaptureVideoDataOutput()
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    
    let videoView = UIView()
    let backView = UIView()
    let iconView = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        prepare()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        prepare()
    }
    
    func prepare() {

        for view in [backView, videoView, iconView] {
            contentView.addSubview(view)
            view.layer.cornerRadius = 12
            view.layer.masksToBounds = true
        }
        
        backView.backgroundColor = .lightGray

        iconView.contentMode = .center
        iconView.image = icon
        videoView.alpha = 0
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            self?.pSessionAndVideoLayer()
            guard let self = self else { return }
            if let layer = self.videoPreviewLayer {
                self.session?.startRunning()
                DispatchQueue.main.async {
                    layer.frame = self.videoView.frame
                    self.videoView.layer.insertSublayer(layer, at: 0)
                    UIView.animate(withDuration: 0.3) { [weak self] in
                        self?.videoView.alpha = 1
                    }
                }
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        videoView.frame = CGRect(
              x: 1,
              y: 1,
              width: bounds.width-2,
              height: bounds.height-2
          )
        backView.frame = videoView.frame
        videoPreviewLayer?.frame = videoView.bounds
        iconView.frame = videoView.bounds
    }
    
    private func pSessionAndVideoLayer() {

        session = AVCaptureSession()
        session?.sessionPreset = AVCaptureSession.Preset.iFrame960x540
        guard   let frontCamera = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .front),
                let input = try? AVCaptureDeviceInput(device: frontCamera) else { return }
        if let session = session, session.canAddInput(input) {
            session.addInput(input)
            videoOutput = AVCaptureVideoDataOutput()
            if let videoOutput = videoOutput, session.canAddOutput(videoOutput) {
                session.addOutput(videoOutput)

                videoPreviewLayer = AVCaptureVideoPreviewLayer(session: session)
                videoPreviewLayer?.videoGravity = .resizeAspectFill
                videoPreviewLayer?.connection?.videoOrientation = .portrait
                videoPreviewLayer?.opacity = 0.8
            }
        }
    }
}
