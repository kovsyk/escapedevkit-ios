//
//  ESCMediaSelector.swift
//  devkit-demo
//
//  Created by Andrey on 06.08.2021.
//  Copyright © 2021 EscapeTech. All rights reserved.
//

import Foundation
import Photos
import UIKit
import AVFoundation
import PhotosUI
import MobileCoreServices

class ESCMediaFetchModule: NSObject, PHPhotoLibraryChangeObserver {
    
    deinit {
        PHPhotoLibrary.shared().unregisterChangeObserver(self)
    }
    
    func photoLibraryDidChange(_ changeInstance: PHChange) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            if let changes = changeInstance.changeDetails(for: self.mediaFetchResult) {
                self.mediaFetchResult = changes.fetchResultAfterChanges
            }
        }
    }
    
    var onFetchResultUpdated: ((PHFetchResult<PHAsset>) -> Void)?
    
    private var mediaFetchResult = PHFetchResult<PHAsset>() {
        didSet {
            onFetchResultUpdated?(mediaFetchResult)
        }
    }
    
    func getFetchResults(mediaTypes: Set<PHAssetMediaType>, fetchLimit: Int = 100, onFetchResultUpdated: ((PHFetchResult<PHAsset>) -> Void)?) {
        PHPhotoLibrary.shared().register(self)
        self.onFetchResultUpdated = onFetchResultUpdated
        mediaFetchResult = PHAsset.fetchAssets(with: self.getFetchOptions(mediaTypes: mediaTypes, fetchLimit: fetchLimit))
    }
    
    private func getFetchOptions(mediaTypes: Set<PHAssetMediaType>, fetchLimit: Int) -> PHFetchOptions {
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key:"creationDate", ascending: false)]
        fetchOptions.fetchLimit = fetchLimit
    
        mediaTypes.forEach({
            let mediaPredicate = NSPredicate(format: "mediaType = %i", $0.rawValue)
            if let storePredicate = fetchOptions.predicate {
                fetchOptions.predicate = NSCompoundPredicate(orPredicateWithSubpredicates: [storePredicate, mediaPredicate])
            } else {
                fetchOptions.predicate = mediaPredicate
            }
        })
        return fetchOptions
    }
    
    func getFetchResultAsset(index: Int) -> PHAsset {
        mediaFetchResult.object(at: index)
    }
    
    func requestFetchResultImage(index: Int, size: CGSize, requestOptions: PHImageRequestOptions, completion: @escaping ((UIImage?) -> Void)) {
        requestImage(asset: getFetchResultAsset(index: index), size: size, requestOptions: requestOptions, completion: completion)
    }
    
    func requestImage(asset: PHAsset, size: CGSize, requestOptions: PHImageRequestOptions, completion: @escaping ((UIImage?) -> Void)) {
        DispatchQueue.global(qos: .background).async {
            PHImageManager.default().requestImage(
                for: asset,
                targetSize: size,
                contentMode: PHImageContentMode.aspectFill,
                options: requestOptions,
                resultHandler: { (image, _) in
                    completion(image)
                })
        }
    }
}

open class ESCMediaSelector: NSObject {
    typealias ESCMedia  = ESCFiles.ESCMediaDescription
    typealias ImageType = ESCFiles.ESCFileTypes.ImageType
    typealias VideoType = ESCFiles.ESCFileTypes.VideoType
    
    class ESCMediaWriteModule: NSObject {
        
        public init(capturedImageType: ImageType) {
            self.capturedImageType = capturedImageType
        }
        
        var capturedImageType: ImageType
        
        public func writeMediaRepresentables(mediaRepresentables: [ESCMediaRepresentable], directoryForSaving: URL?, completion: @escaping ([ESCMedia]) -> Void, onError: @escaping (Error) -> Void) {
            let queue = OperationQueue()
            var results: [ESCMedia] = []
            
            let completionBlock = BlockOperation {
                DispatchQueue.main.async {
                    completion(results)
                }
            }
            
            let captureImageType = self.capturedImageType
            
            for mediaRepresentableItem in mediaRepresentables {
                let operation = ESCAsyncOperation()
                operation.block =  { [weak operation] in
                    mediaRepresentableItem.writeMediaRepresentable(imageType: captureImageType, directoryForSaving: directoryForSaving, completion: { result in
                        switch result {
                        case .failure(let error):
                            queue.cancelAllOperations()
                            onError(error)
                            DispatchQueue.global(qos: .background).async {
                                results.compactMap({$0.url}).forEach({ try? FileManager.default.removeItem(at: $0)})
                            }
                        case .success(let media):
                            results.append(media)
                        }
                        operation?.cancel()
                    })
                }
                completionBlock.addDependency(operation)
                queue.addOperation(operation)
            }
            queue.addOperation(completionBlock)
        }
    }

    let errorHandler: ((Error)-> Void)?
    public var cameraPermissionDeniedHandler: (()->Void)?
    let isWritePhotoToAlbum: Bool
    let mediaFetchModule: ESCMediaFetchModule
    let mediaWriteModule: ESCMediaWriteModule
    let capturedImageType: ImageType
    let capturedVideoType: VideoType
    var pickerDelegate: ESCMediaPickerDelegate?
    var mediaResultingHandler: (([ESCMedia])->Void)?
    
    public required init(isWritePhotoToAlbum: Bool = true,
                         cameraPermissionDeniedHandler: (()->Void)? = nil,
                         errorHandler: ((Error) -> Void)? = nil,
                         capturedImageType: ESCFiles.ESCFileTypes.ImageType = .jpg(compressionQuality: 1),
                         capturedVideoType: ESCFiles.ESCFileTypes.VideoType = .mov(quality: .typeMedium)) {
        self.isWritePhotoToAlbum = isWritePhotoToAlbum
        self.capturedImageType = capturedImageType
        self.capturedVideoType = capturedVideoType
        self.errorHandler = errorHandler
        self.cameraPermissionDeniedHandler = cameraPermissionDeniedHandler
        self.mediaFetchModule = ESCMediaFetchModule()
        self.mediaWriteModule = ESCMediaWriteModule(capturedImageType: capturedImageType)
        super.init()
    }
    
    open func getPHAssets(libraryPermissionDeniedHandler: (()->Void)? = nil,
                          mediaType: MediaType,
                          fetchLimit: Int = 100,
                          onFetchResultUpdated: ((PHFetchResult<PHAsset>) -> Void)?) {
        
        checkLibraryPermission(statusHandler: { [weak self] isDenied in
            guard let self = self else { return }
            if isDenied {
                libraryPermissionDeniedHandler?()
            } else {
                self.mediaFetchModule.getFetchResults(mediaTypes: mediaType.pHAssetMediaTypes, fetchLimit: fetchLimit, onFetchResultUpdated: onFetchResultUpdated)
            }
        })
    }
    
    open func checkCameraPermission(statusHandler: @escaping (_ isDenied: Bool) -> Void) {
        switch AVCaptureDevice.authorizationStatus(for: AVMediaType.video) {
        case .authorized, .notDetermined, .restricted:
            statusHandler(false)
        case .denied:
            statusHandler(true)
        default: ()
        }
    }
    
    open func checkLibraryPermission(statusHandler: @escaping (_ isDenied: Bool) -> Void) {
        PHPhotoLibrary.requestAuthorization({ status in
            switch status {
            case .authorized, .notDetermined, .restricted:
                statusHandler(false)
            case .denied:
                statusHandler(true)
            default: ()
            }
        })
    }
    
    open func requestImage(asset: PHAsset, size: CGSize, requestOptions: PHImageRequestOptions, completion: @escaping ((UIImage?) -> Void)) {
        mediaFetchModule.requestImage(asset: asset, size: size, requestOptions: requestOptions, completion: completion)
    }
    
    open func selectImage(from presentingViewController: UIViewController,
                          directoryForSaving: URL?,
                          sourceType: SourceType,
                          completion: @escaping ([ESCFiles.ESCImageDescription]) -> Void) {
        
        self.pselectMedia(from: presentingViewController,
                          directoryForSaving: directoryForSaving,
                          mediaType: .image,
                          sourceType: sourceType,
                          completion: { mediaArray in
                            completion(mediaArray.compactMap({ ESCFiles.ESCImageDescription(preview: $0.preview, url: $0.url) }))
                          })
    }
    
    open func selectMedia(from presentingViewController: UIViewController,
                          directoryForSaving: URL,
                          mediaType: MediaType,
                          sourceType: SourceType,
                          completion: @escaping ([ESCFiles.ESCCopiedMediaDescription]) -> Void) {
        
        self.pselectMedia(from: presentingViewController,
                          directoryForSaving: directoryForSaving,
                          mediaType: mediaType,
                          sourceType: sourceType,
                          completion: { [weak self] mediaArray in
                            guard mediaArray.filter({ $0.url != nil }).count == mediaArray.count else {
                                self?.errorHandler?(ESCMediaSelectorError.unknownError)
                                return
                            }
                            completion(mediaArray.compactMap({ ESCFiles.ESCCopiedMediaDescription(preview: $0.preview, url: $0.url!, name: $0.name) }))
                          })
    }
    
    open func writeAssetsToUrl(assets: [PHAsset], directoryForSaving: URL, completion: @escaping ([ESCFiles.ESCCopiedMediaDescription]) -> Void) {
        mediaWriteModule.writeMediaRepresentables(
            mediaRepresentables: assets,
            directoryForSaving: directoryForSaving,
            completion: { [weak self] mediaArray in
                guard mediaArray.filter({ $0.url != nil}).count == mediaArray.count else {
                    self?.errorHandler?(ESCMediaSelectorError.unknownError)
                    return
                }
                completion(mediaArray.compactMap({ ESCFiles.ESCCopiedMediaDescription(preview: $0.preview, url: $0.url!, name: $0.name) }))
            }, onError: { [weak self] error in
                self?.errorHandler?(error)
            })
    }
    
    func pselectMedia(from presentingViewController: UIViewController,
                      directoryForSaving: URL?,
                      mediaType: MediaType,
                      sourceType: SourceType,
                      completion: @escaping ([ESCMedia]) -> Void) {
        mediaResultingHandler = completion
        guard pickerDelegate == nil else {
            self.errorHandler?(ESCMediaSelectorError.doublePresentingError)
            return
        }
        let _pickerDelegate = ESCMediaPickerDelegate(mediaSelector: self, directoryForSaving: directoryForSaving)
        _pickerDelegate.onDismiss = { [weak self] in
            self?.pickerDelegate = nil
        }
        pickerDelegate = _pickerDelegate
        
        showPickerController(presentingViewController: presentingViewController, mediaType: mediaType, sourceType: sourceType)
    }
    
    func showPickerController(presentingViewController: UIViewController,
                              mediaType: MediaType,
                              sourceType: SourceType,
                              sourceView: UIView? = nil) {
        switch sourceType {
        case .photoLibrary(let selectionLimit):
            if #available(iOS 14.0, *) {
                self.showPHPickerViewController(presentingViewController: presentingViewController, mediaType: mediaType, selectionLimit: selectionLimit)
            } else {
                self.showImageViewPickerController(presentingViewController: presentingViewController, mediaType: mediaType, sourceType: sourceType, sourceView: sourceView)
            }
        case .camera:
            checkCameraPermission(statusHandler: { [weak self] isDenied in
                DispatchQueue.main.async { [weak self] in
                    if isDenied {
                        self?.cameraPermissionDeniedHandler?()
                    } else {
                        self?.showImageViewPickerController(presentingViewController: presentingViewController, mediaType: mediaType, sourceType: sourceType, sourceView: sourceView)
                    }
                }
            })
        }
    }
    
    func showImageViewPickerController(presentingViewController: UIViewController, mediaType: MediaType, sourceType: SourceType, sourceView: UIView? = nil) {
        let imageViewPickerController = UIDevice.current.userInterfaceIdiom == .pad ? IPadImagePickerController() : UIImagePickerController()
        imageViewPickerController.delegate = pickerDelegate
        imageViewPickerController.sourceType = sourceType.imagePickerSource
        imageViewPickerController.mediaTypes = Array(mediaType.imagePickerMediaTypes)
        imageViewPickerController.videoExportPreset = AVAssetExportPresetPassthrough
        switch self.capturedVideoType {
        case .mov(let quality):
            imageViewPickerController.videoQuality = quality
        }
        if let sourceView = sourceView {
            imageViewPickerController.modalPresentationStyle = .fullScreen
            imageViewPickerController.popoverPresentationController?.sourceView = sourceView
        }
        self.present(vc: imageViewPickerController, presentingViewController: presentingViewController)
    }
    
    @available(iOS 14.0, *)
    private func showPHPickerViewController(presentingViewController: UIViewController, mediaType: MediaType, selectionLimit: Int, sourceView: UIView? = nil) {
        var config = PHPickerConfiguration(photoLibrary: PHPhotoLibrary.shared())
        config.preferredAssetRepresentationMode = .current
        config.selectionLimit = selectionLimit
        config.filter = PHPickerFilter.any(of: Array(mediaType.pickerFilter))
        let pickerViewController = PHPickerViewController(configuration: config)
        pickerViewController.delegate = pickerDelegate
        self.present(vc: pickerViewController, presentingViewController: presentingViewController)
    }
    
    func processImageResult(asset: PHAsset?, image: UIImage, directoryForSaving: URL?) {
        do {
            var url: URL?
            if let directory = directoryForSaving {
                let fileName = ESCFileService.createFileName(extensionType: capturedImageType.extensionType)
                url = try ESCFileService.writeImageToDirectory(image: image, imageType: capturedImageType, directoryForSaving: directory, fileName: fileName)
            }
            self.processPickerResult(mediaArray: [ESCMedia(preview: image, url: url, name: asset?.fileName)])
        } catch let error {
            self.errorHandler?(error)
        }
    }
    
    func processVideoResult(asset: PHAsset?, tempURL: URL, directoryForSaving: URL?) {
        do {
            let fileName = ESCFileService.createFileName(extensionType: tempURL.pathExtension)
            let url = try ESCFileService.copyItemToDirectory(tempUrl: tempURL, directoryForSaving: directoryForSaving!, fileName: fileName)
            let preview = try ESCFileService.createPreviewImage(videoUrl: url)
            self.processPickerResult(mediaArray: [ESCMedia(preview: preview, url: url, name: asset?.fileName)])
        } catch let error {
            self.errorHandler?(error)
        }
    }
    
    func processPickerResult(mediaArray: [ESCMedia]) {
        DispatchQueue.main.async { [weak self] in
            self?.mediaResultingHandler?(mediaArray)
        }
    }
    
    func showAlert(presentingViewController: UIViewController?, title: String, message: String, okTitle: String, okHandler: (() -> Void)? = nil, cancelHandler: ((UIAlertAction) -> Void)? = nil, presentCompletion: (() -> Void)? = nil) {
        let controller = ESCAlertViewController(
            title: title,
            message: message,
            style: .alert,
            actions: [
            ESCAlertAction(title: okTitle,  style: .default, block: {
                okHandler?()
            })
        ])
        presentingViewController?.present(controller, animated: true, completion: presentCompletion)
    }
    
    func present(vc: UIViewController, presentingViewController: UIViewController?) {
        presentingViewController?.present(vc, animated: true)
    }
}


class ESCMediaPickerDelegate: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate, PHPickerViewControllerDelegate {
    
    var onDismiss = { }

    init(mediaSelector: ESCMediaSelector, directoryForSaving: URL?) {
        self.mediaSelector = mediaSelector
        self.directoryForSaving = directoryForSaving
    }
    weak var mediaSelector: ESCMediaSelector?
    var directoryForSaving: URL?
    
    @available(iOS 14.0, *)
    public func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
        picker.dismiss(animated: true)
        mediaSelector?.mediaWriteModule.writeMediaRepresentables(mediaRepresentables: results, directoryForSaving: directoryForSaving, completion:{ [weak self] results in
            self?.mediaSelector?.mediaResultingHandler?(results)
            self?.onDismiss()
        }, onError: { [weak self] error in
            self?.mediaSelector?.errorHandler?(error)
            self?.onDismiss()
        })
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
        onDismiss()
    }
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        let asset = info[.phAsset] as? PHAsset
        if let image = info[.originalImage] as? UIImage {
            if asset == nil && self.mediaSelector?.isWritePhotoToAlbum ?? false {
                var placeholderAsset: PHObjectPlaceholder? = nil
                PHPhotoLibrary.shared().performChanges({
                    placeholderAsset = PHAssetChangeRequest.creationRequestForAsset(from: image).placeholderForCreatedAsset
                }, completionHandler: { [weak self] (success, error) in
                    let asset = PHAsset.fetchAssets(withLocalIdentifiers: [placeholderAsset?.localIdentifier ?? ""], options: nil).firstObject
                    self?.mediaSelector?.processImageResult(asset: asset, image: image, directoryForSaving: self?.directoryForSaving)
                })
            } else {
                self.mediaSelector?.processImageResult(asset: asset, image: image, directoryForSaving: directoryForSaving)
            }
        } else if (info[.mediaType] as? String) == kUTTypeMovie as String {
            if asset == nil && self.mediaSelector?.isWritePhotoToAlbum ?? false  {
                var placeholderAsset: PHObjectPlaceholder? = nil
                PHPhotoLibrary.shared().performChanges({
                    placeholderAsset = PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: info[.mediaURL] as! URL)?.placeholderForCreatedAsset
                }) { [weak self] (success, error) in
                    let asset = PHAsset.fetchAssets(withLocalIdentifiers: [placeholderAsset?.localIdentifier ?? ""], options: nil).firstObject
                    self?.mediaSelector?.processVideoResult(asset: asset, tempURL: info[.mediaURL] as! URL, directoryForSaving: self?.directoryForSaving)
                }
            } else {
                self.mediaSelector?.processVideoResult(asset: asset, tempURL: info[.mediaURL] as! URL, directoryForSaving: directoryForSaving)
            }
        } else {
            mediaSelector?.errorHandler?(ESCMediaSelectorError.unknownError)
        }
        onDismiss()
    }
}


class IPadImagePickerController : UIImagePickerController {
    override var shouldAutorotate: Bool { false }
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask { UIInterfaceOrientationMask.landscape }
}


extension ESCMediaSelector {
    public enum MediaType {
        case video
        case image
        case videoAndImage
        
        @available(iOS 14, *)
        var pickerFilter: Set<PHPickerFilter> {
            switch self {
            case .image:            return [.images]
            case .video:            return [.videos]
            case .videoAndImage:    return [.images, .videos]
            }
        }
        
        var imagePickerMediaTypes: Set<String> {
            switch self {
            case .video:            return [kUTTypeMovie as String]
            case .image:            return [kUTTypeImage as String]
            case .videoAndImage:    return [kUTTypeMovie as String, kUTTypeImage as String]
            }
        }
        
        var pHAssetMediaTypes: Set<PHAssetMediaType> {
            switch self {
            case .image:            return [.image]
            case .video:            return [.video]
            case .videoAndImage:    return [.image, .video]
            }
        }
    }

    
    public enum SourceType {
        case camera
        case photoLibrary(selectionCount: Int)
        
        var imagePickerSource: UIImagePickerController.SourceType {
            switch self {
            case .camera:
                return .camera
            case .photoLibrary:
                return .photoLibrary
            }
        }
    }
}

fileprivate extension PHAsset {
    func extensionType() -> String? {
        let fileName = PHAssetResource.assetResources(for: self).first?.originalFilename
        return URL(string:fileName ?? "")?.pathExtension.lowercased()
    }
}

public enum ESCMediaSelectorError: Error, CustomStringConvertible {
    case wrongAssetFormat
    case requestImageError
    case requestAVAssetError
    case doublePresentingError
    
    case unknownError

    public var description: String {
        switch self {
        case .wrongAssetFormat:
            return "Audio or Unknown PHAssetMediaType"
        case .requestImageError:
            return "Error of requesting image data from the asset"
        case .requestAVAssetError:
            return "Error of requesting avasset from the asset"
        case .doublePresentingError:
            return "Trying to present imagePicker over imagePicker"
        case .unknownError:
            return "Unknown error"
        }
    }
}

protocol ESCMediaRepresentable {
    func writeMediaRepresentable(imageType: ESCMediaSelector.ImageType, directoryForSaving: URL?, completion: @escaping (Result<ESCMediaSelector.ESCMedia, Error>) -> Void)
}

extension PHAsset: ESCMediaRepresentable {
    
    var fileName: String? {
        return PHAssetResource.assetResources(for: self).first?.originalFilename
    }
    
    func writeMediaRepresentable(imageType: ESCMediaSelector.ImageType, directoryForSaving: URL?, completion: @escaping (Result<ESCMediaSelector.ESCMedia, Error>) -> Void) {
        guard let directoryForSaving = directoryForSaving else {
            completion(.failure(ESCMediaSelectorError.unknownError))
            return
        }
        switch mediaType {
        case .video:
            let requestOptions = PHVideoRequestOptions()
            requestOptions.isNetworkAccessAllowed = true
            
            PHImageManager.default().requestAVAsset(forVideo: self, options: requestOptions) { (avasset, _, _) in
                guard let avurlAsset = avasset as? AVURLAsset else {
                    completion(.failure(ESCMediaSelectorError.requestAVAssetError))
                    return
                }
                let tempURL = avurlAsset.url
                ESCFileService.writeVideo(tempURL: tempURL, directoryForSaving: directoryForSaving, completion: completion)
            }
        case .image:
            let requestOptions = PHImageRequestOptions()
            requestOptions.isNetworkAccessAllowed = true
            PHImageManager.default().requestImageData(for: self, options: requestOptions, resultHandler: { (data, uti, orientation, info) in
                do {
                    guard let data = data,
                          let image = UIImage(data: data) else {
                        completion(.failure(ESCMediaSelectorError.requestImageError))
                        return
                    }
                    let fileName = ESCFileService.createFileName(extensionType: imageType.extensionType)
                    let url = try ESCFileService.writeImageToDirectory(image: image, imageType: imageType, directoryForSaving: directoryForSaving, fileName: fileName)
                    completion(.success(ESCMediaSelector.ESCMedia(preview: image, url: url, name: self.fileName)))
                } catch let error {
                    completion(.failure(error))
                }
            })
        default:
            completion(.failure(ESCMediaSelectorError.wrongAssetFormat))
        }
    }
}

@available(iOS 14, *)
extension PHPickerResult: ESCMediaRepresentable {
    func writeMediaRepresentable(imageType: ESCMediaSelector.ImageType, directoryForSaving: URL?, completion: @escaping (Result<ESCMediaSelector.ESCMedia, Error>) -> Void) {
        if itemProvider.canLoadObject(ofClass: UIImage.self) {
            writePHPickerResultImage(imageType: imageType ,directoryForSaving: directoryForSaving, completion: completion)
        } else {
            guard let directory = directoryForSaving else {
                completion(.failure(ESCMediaSelectorError.unknownError))
                return
            }
            writePHPickerResultVideo(directoryForSaving: directory, completion: completion)
        }
    }
    
    func writePHPickerResultVideo(directoryForSaving: URL, completion: @escaping (Result<ESCMediaSelector.ESCMedia, Error>) -> Void) {
        itemProvider.loadFileRepresentation(forTypeIdentifier: UTType.movie.identifier) { tempURL, error in
            guard let tempURL = tempURL else {
                completion(.failure(error ?? ESCMediaSelectorError.unknownError))
                return
            }
            ESCFileService.writeVideo(tempURL: tempURL, directoryForSaving: directoryForSaving, completion: completion)
        }
    }
    
    func writePHPickerResultImage(imageType: ESCMediaSelector.ImageType, directoryForSaving: URL?, completion: @escaping (Result<ESCMediaSelector.ESCMedia, Error>) -> Void) {
        itemProvider.loadObject(ofClass: UIImage.self, completionHandler: { (image, error) in
            guard let image = image as? UIImage else {
                completion(.failure(error ?? ESCMediaSelectorError.unknownError))
                return
            }
            do {
                var url: URL?
                if let directory = directoryForSaving {
                    let fileName = ESCFileService.createFileName(extensionType: imageType.extensionType)
                    url = try ESCFileService.writeImageToDirectory(image: image, imageType: imageType, directoryForSaving: directory, fileName: fileName)
                }
                completion(.success(ESCMediaSelector.ESCMedia(preview: image, url: url, name: itemProvider.suggestedName)))
            } catch let error {
                completion(.failure(error))
            }
        })
    }
}
