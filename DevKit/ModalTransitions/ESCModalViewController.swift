//
//  ModalViewController.swift
//  modal-test
//
//  Created by Igor Ovchinnikov on 31.03.2021.
//

import Foundation
import UIKit

private class PanGestureRecognizer: UIPanGestureRecognizer, UIGestureRecognizerDelegate {
    
    var ignoringViews: [UIView] = []
    
    override init(target: Any?, action: Selector?) {
        super.init(target: target, action: action)
        self.delegate = self
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if let view = touch.view {
            if ignoringViews.contains(view) {
                return false
            }
        }
        return true
    }
    
    override func canPrevent(_ preventedGestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
}


open class ESCModalViewController: UIViewController {
    
    open override var modalPresentationStyle: UIModalPresentationStyle {
        get { .overFullScreen }
        set { }
    }
    
    private var customHeight: CGFloat?
    open var recognizerIgnoredViews: [UIView] { [] }
    open var recognizerViews: [UIView] { [headerView] }
    open var headerHeight: CGFloat { 65.0 }
    open var footerHeight: CGFloat { 72.0 }
    open var containerCornerRadius: CGFloat { 10.0 }
    
    public let contentView = UIView()
    public let headerView = UIView()
    public let footerView = UIView()
    
    var contentViews: [UIView] {
        [contentView, headerView, footerView]
    }
    
    private var sTransitioningDelegate: ViewControllerTransitioningDelegate!
    
    open override var transitioningDelegate: UIViewControllerTransitioningDelegate? {
        get { sTransitioningDelegate }
        set { }
    }
    
    convenience public init(height: CGFloat? = nil) {
        self.init(nibName: nil, bundle: nil, height: height)
    }
    
    public init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?, height: CGFloat? = nil) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        customHeight = height
        prepare()
    }
    
    override public init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        prepare()
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
        prepare()
    }
    
    open func prepare() {
        contentViews.forEach { view.addSubview($0) }
        headerView.backgroundColor = UIColor.black.withAlphaComponent(0.1)
        footerView.backgroundColor = UIColor.black.withAlphaComponent(0.1)
        sTransitioningDelegate = ViewControllerTransitioningDelegate(customHeight: customHeight, containerCornerRadius: containerCornerRadius)
        preparePanRecognizer()
    }
    
    private func  preparePanRecognizer() {
        let panRecognizer = PanGestureRecognizer(target: self, action: #selector(handleClosingSwipe(sender:)))
        panRecognizer.ignoringViews = recognizerIgnoredViews
        recognizerViews.forEach({ $0.addGestureRecognizer(panRecognizer) })
    }
    
    open override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        updateFrames()
    }
    
    open func updateFrames() {
        headerView.frame = CGRect(
            x: 0,
            y: 0,
            width: view.frame.width,
            height: headerHeight
        )
        contentView.frame = CGRect(
            x: 0,
            y: headerView.frame.maxY,
            width: view.frame.width,
            height: view.frame.height - headerHeight - footerHeight
        )
        footerView.frame = CGRect(
            x: 0,
            y: contentView.frame.maxY,
            width: view.frame.width,
            height: footerHeight
        )
    }
    
    private var firstTranslation: CGPoint?
    
    @objc func handleClosingSwipe(sender: UIPanGestureRecognizer) {
        sTransitioningDelegate.handleClosingSwipe(viewController: self, sender: sender)
    }
    
    open func onDismiss() { }
    
    @objc fileprivate func close() {
        onDismiss()
        self.dismiss(animated: true) { }
    }
}

fileprivate class ViewControllerTransitioningDelegate: NSObject, UIViewControllerTransitioningDelegate {
    
    private var firstTranslation: CGPoint?
    var customHeight: CGFloat?
    var containerCornerRadius: CGFloat = 10.0
    
    fileprivate class Interactor: UIPercentDrivenInteractiveTransition {
        var hasStarted = false
        var shouldFinish = false
    }
    
    convenience init(customHeight: CGFloat? = nil, containerCornerRadius: CGFloat = 10) {
        self.init()
        self.customHeight = customHeight
        self.containerCornerRadius = containerCornerRadius
    }
    
    func handleClosingSwipe(viewController: UIViewController, sender: UIPanGestureRecognizer) {
        let view = viewController.view!
        let translation = sender.translation(in: view)
        switch sender.state {
        case .began:
            firstTranslation = translation
            interactor.hasStarted = true
            viewController.dismiss(animated: true, completion: nil)
        case .cancelled, .failed:
            firstTranslation = nil
            interactor.hasStarted = false
            interactor.cancel()
        case .ended:
            firstTranslation = nil
            interactor.hasStarted = false
            if interactor.shouldFinish {
                interactor.finish()
                (viewController as? ESCModalViewController)?.onDismiss()
            } else {
                interactor.cancel()
            }
        default:
            break
        }
        let percentThreshold: CGFloat = 0.3
        
        if let firstTranslation = firstTranslation, translation.y - firstTranslation.y > 0 {
            let downShift = (translation.y - firstTranslation.y) / (view.frame.height - 20)
            let progress = CGFloat(downShift)
            switch sender.state {
            case .changed:
                interactor.shouldFinish = progress > percentThreshold
                interactor.update(progress)
            default:
                break
            }
        }
    }
    
    private var interactor = Interactor()
    
    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        Presenter(customHeight: customHeight, containerCornerRadius: containerCornerRadius)
    }

    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        Dismisser(interactor: interactor)
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        interactor.hasStarted ? interactor : nil
    }
    
    private class CustomVisualEffectView: UIView {
        override init(frame: CGRect) {
            super.init(frame: frame)
            backgroundColor = .black
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }

    private class Presenter: NSObject, UIViewControllerAnimatedTransitioning {
        
        var duration: TimeInterval { 0.4 }
        var customHeight: CGFloat?
        var containerCornerRadius: CGFloat = 10.0
        
        init(customHeight: CGFloat?, containerCornerRadius: CGFloat) {
            self.customHeight = customHeight
            self.containerCornerRadius = containerCornerRadius
        }

        func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
            duration
        }

        func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
            let container = transitionContext.containerView
            let backgroundView = CustomVisualEffectView()
            backgroundView.alpha = 0.0
            backgroundView.frame = container.frame
            container.addSubview(backgroundView)
            if let toViewController = transitionContext.viewController(forKey: .to) as? ESCModalViewController {
                let tapRecognizer = UITapGestureRecognizer(target: toViewController, action: #selector(ESCModalViewController.close))
                tapRecognizer.cancelsTouchesInView = false
                backgroundView.addGestureRecognizer(tapRecognizer)
            }

            let toView = transitionContext.view(forKey: .to)!
            do {
                let w = min(toView.frame.width, 500)
                let h = customHeight ?? (toView.frame.height - 40.0 - container.safeAreaInsets.top)
                toView.frame = CGRect(
                    x: toView.frame.origin.x + (toView.frame.width - w)/2,
                    y: container.frame.height - h,
                    width: w,
                    height: h
                )
                container.addSubview(toView)
            }
            do {
                toView.layer.masksToBounds = true
                toView.layer.cornerRadius = containerCornerRadius
                toView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            }
            do {
                container.layoutIfNeeded()
                let toViewOriginYBefore = toView.frame.origin.y
                toView.frame.origin.y = container.frame.height
                UIView.animate(withDuration: duration,
                               delay: 0,
                               usingSpringWithDamping: 0.9,
                               initialSpringVelocity: 0,
                               options: [],
                               animations: {
                    toView.frame.origin.y = toViewOriginYBefore
                    backgroundView.alpha = 0.5
                }) { (completed) in
                    transitionContext.completeTransition(completed)
                }
            }
        }
    }

    private class Dismisser: NSObject, UIViewControllerAnimatedTransitioning {
        
        var interactor: Interactor?
        
        init(interactor: Interactor?) {
            self.interactor = interactor
        }
        
        var duration: TimeInterval {
            interactor?.hasStarted == true ? 0.5 : 0.3
        }
        
        func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
            duration
        }

        func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
            let container = transitionContext.containerView
            let backgroundView = container.subviews.first { $0 is CustomVisualEffectView }
            let fromView = transitionContext.view(forKey: .from)!
            container.layoutIfNeeded()
            do {
                UIView.animate(withDuration: duration,
                               delay: 0,
                               options: .curveLinear,
                               animations: {
                    fromView.frame.origin.y = container.frame.height
                    backgroundView?.alpha = 0.0
                }, completion: { (completed) in
                    transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
                })
            }
        }
    }
}
