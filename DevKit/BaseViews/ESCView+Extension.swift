//
//  ESCView+Extension.swift
//  EscDevKit
//
//  Created by EscapeTech on 17.02.2021.
//  Copyright © 2021 EscapeTech. All rights reserved.
//

import UIKit

struct ViewWithWH {
    var view: UIView
    var w: CGFloat? = nil
    var h: CGFloat? = nil
}

extension UIView {
    func configureViewsInStackAndGetHeight(verticalSpacing: CGFloat = 16,
                                    viewWithWhArray: [ViewWithWH],
                                    isNeedSetFrames: Bool = true) -> CGFloat {
        var currentHeight:CGFloat = 0
        
        for item in viewWithWhArray {
            var itemSize: CGSize = item.view.frame.size
            if let width = item.w,
               let height = item.h {
                itemSize = CGSize(width: width, height: height)
            }
            if item.h == nil, let width = item.w {
                itemSize = item.view.sizeThatFits(CGSize(width: width, height: CGFloat.greatestFiniteMagnitude))
                itemSize.width = width
            }
          
            if isNeedSetFrames {
                item.view.frame.size = itemSize
                item.view.center = self.center
                item.view.frame.origin.y = currentHeight
                if !item.view.frame.height.isZero && !item.view.isHidden {
                    item.view.frame.origin.y += verticalSpacing
                }
                currentHeight = item.view.frame.maxY
            } else {
                currentHeight += itemSize.height + verticalSpacing
            }
        }
        
        if !currentHeight.isZero {
            currentHeight += verticalSpacing
        }
        if isNeedSetFrames {
            frame.size.height = currentHeight
        }
        return currentHeight
    }
}
