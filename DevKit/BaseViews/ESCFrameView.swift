//
//  ESCFrameView.swift
//  EscDevKit
//
//  Created by EscapeTech on 17/07/2019.
//  Copyright © 2019 EscapeTech. All rights reserved.
//

import Foundation
import UIKit

public enum ESCFrameViewStyle {
    case circle
    case square
}

public struct FramePath {
    public let backgroundLayerPath: CGPath
    public let lineLayerPath: CGPath
    
    public init(backgroundLayerPath: CGPath, lineLayerPath: CGPath) {
        self.lineLayerPath = lineLayerPath
        self.backgroundLayerPath = backgroundLayerPath
    }
}

open class ESCFrameView: UIView {
    
    let backgroundLayer = CAShapeLayer()
    let lineLayer       = CAShapeLayer()
    
    public var openedFrame: CGRect {
        return CGRect(
            origin: CGPoint(x: (frame.width    - frameSize.width)/2,
                            y: (frame.height   - frameSize.height)/2),
            size: frameSize
        )
    }
    
    public var frameSize: CGSize = .zero {
        didSet { setNeedsLayout() }
    }
    
    public var strokeColor = UIColor.white {
        didSet { changeLayerColor() }
    }
    
    public var lineWidth: CGFloat = 4 {
        didSet { setNeedsLayout() }
    }
    
    public var fillRule: CAShapeLayerFillRule = .evenOdd {
        didSet { changeLayerColor() }
    }
    
    public var fillColor: UIColor = UIColor.black.withAlphaComponent(0.5) {
        didSet { changeLayerColor() }
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.pLayers()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func pLayers() {
        self.changeLayerColor()
        layer.addSublayer(backgroundLayer)
        layer.addSublayer(lineLayer)
    }
    
    fileprivate func changeLayerColor() {
        backgroundLayer.isOpaque = false
        backgroundLayer.fillColor = fillColor.cgColor
        backgroundLayer.fillRule = fillRule
        
        lineLayer.fillColor = UIColor.clear.cgColor
        lineLayer.lineWidth = lineWidth
        lineLayer.strokeColor = strokeColor.cgColor
    }
    
    
    open override func layoutSubviews() {
        backgroundLayer.frame = layer.bounds
        lineLayer.frame = layer.bounds
        let paths = self.pPaths()
        backgroundLayer.path = paths.backgroundLayerPath
        lineLayer.path = paths.lineLayerPath
    }
    
    open func pPaths(size: CGSize? = nil) -> FramePath {
        let layerWidth = layer.bounds.width
        let layerHeight = layer.bounds.height
        let pathWidth = size != nil ? size!.width : frameSize.width
        let pathHeight = size != nil ? size!.height : frameSize.height
        let pathX = (layerWidth - pathWidth) / 2
        let pathY = (layerHeight - pathHeight) / 2
        
        let pbath = self.pPath(
            x: pathX,
            y: pathY,
            width: pathWidth,
            height: pathHeight
        )
        
        let bgPath = UIBezierPath(rect: layer.bounds)
        bgPath.append(pbath)
        bgPath.close()
        
        let linePath = pPath(
            x: pathX - lineWidth/2,
            y: pathY - lineWidth/2,
            width: pathWidth + lineWidth/2,
            height: pathHeight + lineWidth/2
        )
        
        return FramePath(backgroundLayerPath: bgPath.cgPath,
                         lineLayerPath: linePath.cgPath)
    }
    
    open func pPath(x: CGFloat, y: CGFloat, width: CGFloat, height: CGFloat) -> UIBezierPath {
        return UIBezierPath(
            rect: CGRect(
                x: x,
                y: y,
                width: width,
                height: height
            )
        )
    }
}

public class ESCOvalView: ESCFrameView {
    
    open override func pPath(x: CGFloat, y: CGFloat, width: CGFloat, height: CGFloat) -> UIBezierPath {
        return UIBezierPath(
            ovalIn: CGRect(
                x: x,
                y: y,
                width: width,
                height: height
            )
        )
    }
}
