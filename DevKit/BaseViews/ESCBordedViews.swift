//
//  ESCBordedViews.swift
//  EscDevKit
//
//  Created by EscapeTech on 17/07/2019.
//  Copyright © 2019 EscapeTech. All rights reserved.
//

import Foundation
import UIKit

public protocol ESCBordedViewInterface: UIView {
    var cornerRadius: CGFloat { get set }
    var borderWidth: CGFloat { get set }
    var borderColor: UIColor? { get set }
}

public extension ESCBordedViewInterface {
    var cornerRadius: CGFloat {
        get {
            layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
            setNeedsLayout()
        }
    }
    
    var borderWidth: CGFloat {
        set {
            layer.borderWidth = borderWidth
        }
        get {
            layer.borderWidth
        }
    }
    
    var borderColor: UIColor? {
        set {
            layer.borderColor = borderColor?.cgColor
        }
        get {
            guard let borderColor = layer.borderColor else { return nil }
            return UIColor(cgColor: borderColor)
        }
    }
}

open class ESCBordedView: UIView, ESCBordedViewInterface { }

open class ESCBordedButton: UIButton, ESCBordedViewInterface {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        prepareTargets()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        prepareTargets()
    }
    
    func prepareTargets() {
        addTarget(self, action: #selector(_highlightBoarder),   for: .touchDown)
        addTarget(self, action: #selector(_highlightBoarder),   for: .touchDragEnter)
        addTarget(self, action: #selector(_unhighlightBoarder), for: .touchUpInside)
        addTarget(self, action: #selector(_unhighlightBoarder), for: .touchUpOutside)
        addTarget(self, action: #selector(_unhighlightBoarder), for: .touchDragExit)
    }
    
    @IBInspectable var highlightedBorderColor: UIColor?
    
    @objc func _highlightBoarder() {
        highlightBoarder()
    }
    
    @objc func _unhighlightBoarder() {
        unhighlightBoarder()
    }
    
    open func highlightBoarder() {
        layer.borderColor = highlightedBorderColor?.cgColor
    }
    
    open func unhighlightBoarder() {
        layer.borderColor = borderColor?.cgColor
    }
}

open class ESCBarStyledBordedButton: ESCBordedButton {
    
    public var interactiveTitleColor: UIColor?
    public var interactiveBackgroundColor: UIColor? = .black
    public var disabledTitleColor: UIColor?
    public var disabledBackgroundColor: UIColor?
    public var defaultTitleColor: UIColor?
    
    public var defaultBackgroundColor: UIColor? = .white {
        didSet {
            backgroundColor = defaultBackgroundColor
        }
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        prepareButton()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        prepareButton()
    }
    
    public override func didMoveToSuperview() {
        super.didMoveToSuperview()
        prepareButton()
    }
    
    public func prepareButton() {
        setTitleColor(defaultTitleColor, for: .normal)
        setTitleColor(interactiveTitleColor, for: .highlighted)
        setTitleColor(disabledTitleColor ?? interactiveTitleColor?.withAlphaComponent(0.23), for: .disabled)
        backgroundColor = isEnabled ? defaultBackgroundColor    : disabledBackgroundColor ?? defaultBackgroundColor?.withAlphaComponent(0.1)
        tintColor       = isEnabled ? defaultTitleColor         : disabledTitleColor
    }
    
    open override func highlightBoarder() {
        UIView.animate(withDuration: 0.1) {
            self.layer.borderColor  = self.highlightedBorderColor?.cgColor
            self.backgroundColor    = self.interactiveBackgroundColor
        }
    }
    
    open override func unhighlightBoarder() {
        UIView.animate(withDuration: 0.1) {
            self.layer.borderColor  = self.borderColor?.cgColor
            self.backgroundColor    = self.defaultBackgroundColor
        }
    }
    
    override open var isEnabled: Bool {
        didSet {
            backgroundColor = isEnabled ? defaultBackgroundColor : disabledBackgroundColor ?? defaultBackgroundColor?.withAlphaComponent(0.1)
            tintColor = isEnabled ? defaultTitleColor : disabledTitleColor
        }
    }
}
