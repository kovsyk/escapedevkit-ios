//
//  ESCFileService.swift
//  devkit-demo
//
//  Created by Andrey on 27.08.2021.
//  Copyright © 2021 EscapeTech. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

public struct ESCFileService {
    private init() { }
    
    public static func copyItemToDirectory(tempUrl: URL, directoryForSaving: URL, fileName: String) throws -> URL {
        let fileURL = directoryForSaving.appendingPathComponent(fileName)
        try FileManager.default.copyItem(at: tempUrl, to: fileURL)
        return fileURL
    }
    
    public static func createFileName(extensionType: String) -> String { "\(UUID().uuidString).\(extensionType)" }
    
    static func writeImageToDirectory(image: UIImage, imageType: ESCFiles.ESCFileTypes.ImageType, directoryForSaving: URL, fileName: String) throws -> URL {
        let data: Data
        switch imageType {
        case .png:
            let rotatedImage = image.rotateToUpOrientation()
            guard let pngData = rotatedImage?.pngData() else { throw ESCFileServiceError.gettingPngData }
            data = pngData
        case .jpg(compressionQuality: let quality):
            guard let jpegData = image.jpegData(compressionQuality: quality) else { throw ESCFileServiceError.gettingJpegData }
            data = jpegData
        }
        let fileURL = directoryForSaving.appendingPathComponent(fileName)
        try data.write(to: fileURL)
        return fileURL
    }
    
    static func createPreviewImage(videoUrl: URL) throws -> UIImage {
        let asset = AVURLAsset(url: videoUrl)
        let generator = AVAssetImageGenerator(asset: asset)
        generator.appliesPreferredTrackTransform = true
        let cgImage = try generator.copyCGImage(at: CMTime(seconds: 0, preferredTimescale: 60), actualTime: nil)
        return UIImage(cgImage: cgImage)
    }
    
    static func writeVideo(tempURL: URL, directoryForSaving: URL, completion: @escaping (Result<ESCFiles.ESCMediaDescription, Error>) -> Void) {
        do {
            let fileName = ESCFileService.createFileName(extensionType: tempURL.pathExtension)
            let url = try ESCFileService.copyItemToDirectory(tempUrl: tempURL, directoryForSaving: directoryForSaving, fileName: fileName)
            let preview = try ESCFileService.createPreviewImage(videoUrl: url)
            completion(.success(ESCFiles.ESCMediaDescription(preview: preview, url: url, name: tempURL.lastPathComponent)))
        } catch let error {
            completion(.failure(error))
        }
    }
}


public enum ESCFileServiceError: Error, CustomStringConvertible {
    case gettingJpegData
    case gettingPngData
    
    public var description: String {
        switch self {
        case .gettingJpegData:
            return "Error getting image as JPEG. Image has no CGImageRef or invalid bitmap format"
        case .gettingPngData:
            return "Error getting image as PNG. Image has no CGImageRef or invalid bitmap format"
        }
    }
}


fileprivate extension UIImage {
    func rotateToUpOrientation()-> UIImage? {
        if (imageOrientation == .up ) { return self }
        UIGraphicsBeginImageContext(self.size)
        draw(in: CGRect(origin: .zero, size: self.size))
        let copy = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return copy
    }
}

public struct ESCFiles {
    private init() {}

    public struct ESCImageDescription {
        init(preview: UIImage, url: URL? = nil) {
            self.preview = preview
            self.url = url
        }
        
        public var preview: UIImage
        public var url: URL?
    }
    
    public struct ESCCopiedMediaDescription {
        public init(preview: UIImage, url: URL, name: String?) {
            self.preview = preview
            self.url = url
            self.name = name
        }
        
        public var preview: UIImage
        public var url: URL
        public var name: String?
    }
    
    struct ESCMediaDescription {
        public init(preview: UIImage, url: URL?, name: String?) {
            self.preview = preview
            self.url = url
            self.name = name
        }
        
        public var preview: UIImage
        public var url: URL?
        public var name: String?
    }

    public struct ESCFileTypes {
        private init() {}
        public enum VideoType {
            case mov(quality: UIImagePickerController.QualityType)
            
            var extensionType: String {
                switch self {
                case .mov:
                    return "mov"
                }
            }
        }
        
        public enum ImageType {
            case png
            case jpg(compressionQuality: CGFloat)
            
            var extensionType: String {
                switch self {
                case .png:
                    return "png"
                case .jpg:
                    return "jpg"
                }
            }
        }
    }
}
