//
//  ESCLogger.swift
//  devkit-demo
//
//  Created by Andrey on 20.08.2021.
//  Copyright © 2021 EscapeTech. All rights reserved.
//

import Foundation

enum ESCLogLevel: Int, Comparable {
    
    static func < (lhs: ESCLogLevel, rhs: ESCLogLevel) -> Bool { lhs.rawValue < rhs.rawValue }
    case none, verbose, debug, info, warning, error
    
    var description: String{
        switch self{
        case .verbose:  return "VERBOSE"
        case .debug:    return "DEBUG"
        case .info:     return "INFO"
        case .warning:  return "WARKNING"
        case .error:    return "ERROR"
        case .none:     return "NONE"
        }
    }
}

class ESCLogger {
    static let main = ESCLogger()
    private static let timeFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.timeStyle = .short
        return formatter
    }()
    func addMessage(_ message: Any?, logLevel: ESCLogLevel = .debug, file: String = #file, line: Int = #line, function: String = #function) {
        #if ESCPRINTLOG
        let _time = ESCLogger.timeFormatter.string(from: Date())
        let _level = logLevel.description
        let _filename = (file as NSString).lastPathComponent
        let _message = message ?? ""
        print("\n[\(_time) \(_level) ] <\(_filename)|\(function):\(line)>: \(_message)")
        #endif
    }
}
