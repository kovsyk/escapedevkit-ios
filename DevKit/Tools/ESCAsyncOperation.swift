//
//  ESCAsyncOperation.swift
//  EscDevKit
//
//  Created by EscapeTech on 11.06.2021.
//  Copyright © 2021 EscapeTech. All rights reserved.
//

import Foundation

public class ESCAsyncOperation: Operation {
    
    public override class func automaticallyNotifiesObservers(forKey key: String) -> Bool { true }
    
    public init(mainBlock: (() -> Void)? = nil) {
        self.block = mainBlock
    }
    
    public var block: (() -> Void)?
    
    enum State: String {
        case ready, executing, finished
        fileprivate var keyPath: KeyPath<ESCAsyncOperation, Bool> {
            switch self {
            case .ready:        return \.isReady
            case .executing:    return \.isExecuting
            case .finished:     return \.isFinished
            }
        }
    }
    
    var state: State = State.ready {
        willSet {
            self.willChangeValue(for: state.keyPath)
            self.willChangeValue(for: newValue.keyPath)
        }
        didSet {
            self.didChangeValue(for: oldValue.keyPath)
            self.didChangeValue(for: state.keyPath)
        }
    }
    
    public override var isReady         : Bool { super.isReady && state == .ready }
    public override var isExecuting     : Bool { state == .executing }
    public override var isFinished      : Bool { state == .finished }
    public override var isAsynchronous  : Bool { true }
    
    public override func start() {
        if isCancelled {
            state = .finished
            return
        }
        state = .executing
        main()
    }
    
    private var canceling = false
    
    public override func cancel() {
        if isExecuting {
            state = .finished
        }
        else {
            canceling = true
            state = .executing
            state = .finished
        }
        block = nil
    }
    
    public
    override func main() {
        super.main()
        if !canceling {
            block?()
        }
        block = nil
    }
}
