//
//  NavigationBarExchangeable.swift
//  EscDevKit
//
//  Created by EscapeTech on 18.12.2020.
//  Copyright © 2020 EscapeTech. All rights reserved.
//

import Foundation
import UIKit

public protocol NavigationBarExchangeable where Self: UIViewController {
    var navigationTitleColor: UIColor? { get }
    var navigationTintColor: UIColor? { get }
    var navigationTitleFont: UIFont? { get }
    var navigationBarTintColor: UIColor? { get }
    var separatorColor: UIColor? { get }
    var barIsHidden: Bool { get }
}

open class ExchangeableNavigationController: UINavigationController {
    private var rootViewController: UIViewController? { viewControllers.first }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        self.setNeedsStatusBarAppearanceUpdate()
        setNavigationBarExchange(viewControllers.first as? NavigationBarExchangeable)
        setNavigationBarHidden(viewControllers.first as? NavigationBarExchangeable)
    }
    
    open override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        let exchange = viewController as? NavigationBarExchangeable
        setNavigationBarExchange(exchange)
        super.pushViewController(viewController, animated: animated)
        setNavigationBarHidden(exchange)
    }
    
    open override func popViewController(animated: Bool) -> UIViewController? {
        let previousViewController = viewControllers.count > 1 ? viewControllers[viewControllers.count - 2] : nil
        setNavigationBarExchange(previousViewController as? NavigationBarExchangeable)
        let popViewController = super.popViewController(animated: animated)
        animatePopTransition()
        return popViewController
    }
    
    open override func popToRootViewController(animated: Bool) -> [UIViewController]? {
        setNavigationBarExchange(rootViewController as? NavigationBarExchangeable)
        let popViewControllers = super.popToRootViewController(animated: animated)
        animatePopTransition()
        return popViewControllers
    }
    
    private func animatePopTransition() {
        transitionCoordinator?.animate(alongsideTransition: { [weak self] context in
            guard let exchange = self?.topViewController as? NavigationBarExchangeable else { return }
            self?.setNavigationBarExchange(exchange)
        }, completion: { [weak self] context in
            guard let exchange = self?.topViewController as? NavigationBarExchangeable else { return }
            self?.setNavigationBarHidden(exchange)
            self?.setNavigationBarExchange(exchange)
        })
        setNavigationBarHidden(topViewController as? NavigationBarExchangeable)
    }
}
    
extension UINavigationController {
    
    fileprivate func setNavigationBarHidden(_ exchange: NavigationBarExchangeable?, animated: Bool = true) {
        if let exchange = exchange {
            setNavigationBarHidden(exchange.barIsHidden, animated: animated)
        }
    }
    
    fileprivate func setNavigationBarExchange(_ exchange: NavigationBarExchangeable?) {
        if let exchange = exchange {
            setNavigationBarExchange(exchange)
        }
    }
    
    open func setNavigationBarExchange(_ exchange: NavigationBarExchangeable) {
        
        var attributes: [NSAttributedString.Key : Any] = [:]
        if let tintColor = exchange.navigationTitleColor {
            attributes[.foregroundColor] = tintColor
        }
        if let font = exchange.navigationTitleFont {
            attributes[.font] = font
        }
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            
            appearance.titleTextAttributes = attributes
            appearance.backgroundColor = exchange.navigationBarTintColor
            navigationBar.tintColor = exchange.navigationTintColor
            appearance.shadowColor = exchange.separatorColor

            navigationBar.standardAppearance = appearance
            navigationBar.scrollEdgeAppearance = appearance
            navigationBar.compactAppearance = appearance
            
        } else {
            if let tintColor = exchange.navigationTintColor {
                navigationBar.tintColor = tintColor
            }
            navigationBar.titleTextAttributes = attributes
            navigationBar.barStyle = .black
            navigationBar.backgroundColor = exchange.navigationBarTintColor
            navigationBar.barTintColor = exchange.navigationBarTintColor
            navigationBar.shadowImage = exchange.separatorColor?.as1ptImage()
            navigationBar.setBackgroundImage(exchange.navigationBarTintColor?.as1ptImage(), for: .default)
        }
    }
}

private extension UIColor {
    func as1ptImage() -> UIImage {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        guard let ctx = UIGraphicsGetCurrentContext() else { return UIImage() }
        self.setFill()
        ctx.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        guard let image = UIGraphicsGetImageFromCurrentImageContext() else { return UIImage() }
        UIGraphicsEndImageContext()
        return image
    }
}
